DROP TABLE IF EXISTS `climbs`;
DROP TABLE IF EXISTS `climb_points`;
DROP TABLE IF EXISTS `climb_slopes`;

CREATE TABLE `climbs` (
	id int auto_increment,
	name varchar(100),
	dbname varchar(60),
	slope varchar(15),
	region int,
	description text,
	comments text,
	author varchar(40),
	points double,
	start_x double,
	start_y double,
	end_x double,
	end_y double,
	stravaseg unsigned bigint,
	primary key(id)
);

CREATE TABLE `regions` (
	id int auto_increment,
	name varchar(45),
	description text,
	center_x double,
	center_y double,
	zoom int,
	primary key(id)
);

CREATE TABLE `climb_points` (
	climb_id int,
	point_nr int,
	point_x double,
	point_y double
);

CREATE TABLE `climb_slopes` (
	climb_id int,
	point_distance double,
	elevation double
);
