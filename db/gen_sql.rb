require 'digest/sha1'

module Altimetr

	# Generate sql for a climb (climb config string)
	# FOR SQLITE3
	def self.generate_climb_sql(str, id, dbname)
		dbname = dbname.gsub(".txt","")
		s = ""
		# Generate climb data sql
		name = str.split("name=")[1].split("slope=")[0]
		name = name.strip.gsub("\'","\'\'")
		# Nachylenie podjazdu
		slope = str.split("slope=")[1].split("points=")[0]
		slope = slope.strip.gsub("\'","\'\'")
		# Punkty podjazdu
		points = str.split("points=")[1].split("region=")[0]
		points = points.strip
		# Region podjazdu
		region = str.split("region=")[1].split("description=")[0]
		region = region.strip.gsub("\'","\'\'")
		# Opis podjazdu
		description = str.split("description=")[1].split("comments=")[0]
		description = description.strip.gsub("\'","\'\'")
		# Komentarze
		comments = str.split("comments=")[1].split("author=")[0]
		comments = comments.strip.gsub("\'","\'\'")
		# Autor podjazdu
		author = str.split("author=")[1].split("\n")[0]
		author = author.strip.gsub("\'","\'\'")
		# Remove author links
		if author.include?("<a")
			author = author.split("(")[0]
		end
		# Strava segment
		stravaseg = str.split("segment=")[1].split("locations=")[0]
		stravaseg = stravaseg.strip
		podstr = str.split("podstawa=")[1].split("daneflot=")[0]
		startstr = podstr.split("start',")[1].split("]")[0]
		startstr = startstr.split(",")
		metastr = podstr.split("meta',")[1].split("]")[0]
		metastr = metastr.split(",")
		startx = startstr[0]
		starty = startstr[1]
		metax = metastr[0]
		metay = metastr[1]
		s << "insert into climbs (`id`, `name`, `dbname`, `slope`, `region`, `description`, `comments`, `author`, `points`, `start_x`, `start_y`, `end_x`, `end_y`, `stravaseg`) values\n(#{id},\'#{name}\',\'#{dbname}\',\'#{slope}\',\'#{region}\',\'#{description}\',\'#{comments}\',\'#{author}\', #{points}, #{startx}, #{starty}, #{metax}, #{metay}, #{stravaseg});\n"
		# Generate locations sql
		locations2 = "locations="+str.split("locations=")[1].split("podstawa=")[0].strip
		locations2[-2] = ""
		locations = []
		eval(locations2)
		s << "insert into climb_points(`climb_id`, `point_nr`, `point_x`, `point_y`) values\n"
		# locations2 after eval generate locations array!!!
		locations.each{|x|
			s << "(#{id},#{x[0]},#{x[1]},#{x[2]}),\n"
		}
		s[-2]=";"
		# Generate slopes sql
		#slopes2 after eval generate slopes array!!!
		slopes2 = "slopes="+str.split("daneflot=")[1].strip
		slopes2.gsub(",]","]")
		eval(slopes2)
		s << "insert into climb_slopes(`climb_id`, `point_distance`, `elevation`) values\n"
		slopes = [] if slopes == nil
		slopes.each{|x|
			distance = x[0].to_s.gsub("\x20","")
			elev = x[1].to_s.gsub("\x20","")
			s << "(#{id}, #{distance}, #{elev}),\n"
		}
		s[-2] = ";"
		return s
	end
	
	# pobierz id podjazdu
	def self.get_id(command, hashfilestr)
		command = command.gsub(".txt","").tr("\n\t\x20","").to_s
		hashfilestr.each_line{|x|
			y = x.split("^")
			if y[1].tr("\n\t\x20","") == command
				#print "Id for #{command} = #{y[0]}\n"
				return y[0]
			end
		}
		print "Fail to get id for #{command}\n"
		return 0
	end
	
	# wygeneruj sql dla wszystkich podjazdów
	def self.generate_all_climbs_sql
		result = ""
		hashfilestr = File.read("tmp/climbhashes.txt")
		files = []
		Dir.foreach("climbs"){|x|
			next if x == "."
			next if x == ".."
			files << x
		}
		files.each{|x|
			id = get_id(x, hashfilestr)
			filename = "climbs/#{x}"
			result << generate_climb_sql(File.read(filename), id, x)
		}
		result[-2] = ";"
		# Add table creation
		result = File.read("tmp/database.sql")+"\n"+generate_regions_sql+"\n"+result
		File.open("tmp/altimetrdb.sql","wb"){|w| w.write(result) }
		return result
	end
	
	def self.get_region_data(name)
		data = File.read("regions/#{name}.txt")
		description = data.split("description=")[1].to_s
		description = description.split("zoom=")[0].to_s
		description.tr!("\n\r\t","")
		zoom = data.split("zoom=")[1].to_s
		zoom = zoom.split("center_x=")[0].to_s
		zoom.tr!("\n\r\t","")
		center_x = data.split("center_x=")[1].to_s
		center_x = center_x.split("center_y=")[0].to_s
		center_x.tr!("\n\r\t","")
		center_y = data.split("center_y=")[1].to_s
		center_y = center_y.split("\n")[0].to_s
		center_y.tr!("\n\r\t","")
		return [description, center_x, center_y, zoom]
	end
	
	def self.sql_trunc(str)
		return str.gsub("'","''")
	end
	
	# wygeneruj sql dla listy regionów (bez opisów!!)
	def self.generate_regions_sql
		sql = "insert into regions(id, name, description, center_x, center_y, zoom) values\n"
		regionslist = File.read("tmp/regions.txt")
		regionslist.each_line{|line|
			# Remove unwanted chars
			line = line.tr("\n\t\r","")
			# remove blank lines
			next if line == ""
			# split id and name
			x = line.split("^")
			# format to sqlite apostrophes
			name = sql_trunc(x[1].tr("\n\t\r",""))
			dbname = sql_trunc(x[2].tr("\n\t\r",""))
			#name = x[1].gsub("',''")
			region_data = get_region_data(dbname)
			sql += "(#{x[0]},'#{name}','#{sql_trunc(region_data[0])}', #{region_data[1]}, #{region_data[2]}, #{region_data[3]}),\n"
		}
		# remove \n and last semicolon
		sql[-2] = ";"
		return sql
	end
	
end

begin
	print "Generowanie bazy danych SQL..\n"
	Altimetr.generate_all_climbs_sql
	print "Generowanie bazy danych SQLite..\n"
	File.delete("tmp/altimetr.db") if File.exist?("tmp/altimetr.db")
	system("sqlite3 tmp/altimetr.db -init tmp/altimetrdb.sql")
end
