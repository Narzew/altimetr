require 'open-uri'
require 'digest/sha1'

$climbhashes = ""

def clear_html(x)
	return x.gsub(/<[^>]+>/, "")
end

def add_warning(x)
	$warnings << "#{x}\n"
	print "Warning: #{x}\n"
end

def add_error(x)
	print "Error: #{x}\n"
	exit
end

$warnings = "" # Lista błędów
$localbase = false

module Altimetr

	$regionlist = File.read("tmp/regions.txt") if File.exist?("tmp/regions.txt")
	# Lista regionów o zmienionych nazwach
	$regionfixlist = File.read("tmp/regions_fix.txt") if File.exist?("tmp/regions.txt")

	def self.download_orig_db
		open("http://altimetr.pl/baza-podjazdow.html"){|s|
			File.open("tmp/db_html.txt","wb"){|w| w.write(s.read) }
		}
		print "Lista podjazdow pobrana.\n"
	end
	
	def self.get_region_id(regionname)
		regionname_old = regionname
		regionname = regionname.tr("\n\t\r\x20","")
		$regionlist.each_line{|line|
			x = line.split("^")
			return x[0] if x[1].tr("\n\t\r\x20","") == regionname
		}
		# Region fixes data
		print "Info: Uzycie regions_fix dla: #{regionname_old}\n"
		$regionfixlist.each_line{|line|
			x = line.split("^")
			x[1] = x[1].tr("\n\t\r","")
			$regionlist.each_line{|line2|
				y = line2.split("^")
				y[2] = y[2].tr("\n\t\r","")
				# Pobierz id regionu z poprawioną nazwą
				if y[2] == x[0]
					return get_region_id(y[1])
				end
			}
		}
		# No valid region found
		add_warning("#{regionname} niezdefiniowany.")
		return 0
	end

	def self.download_links
		# Read from file
		data = File.read("tmp/db_html.txt")
		# Remove mess before db
		data = data.split("<div id=\"baza\" class=\"baza\">")[1]
		# Remove mess after db
		data = data.split("</div>")[0]
		# Remove blank signs
		data = data.tr("\x20\t","")
		# Create link database
		links = []
		# Each line - check that line is region link
		data.each_line{|line|
			next if line.tr("\x20","") == "" # Remove blank lines
			next if line.include?("<h4>") # Remove region links
			# Remove all that isn't a link
			line2 = line.split("<p><ahref=\"")[1] # Remove link start
			line2 = line2.to_s.split("\">")[0]
			links << line2
		}
		links.compact!
		# DEBUG: save to file
		linksstr = ""
		links.each{|x| linksstr << "#{x}\n" }
		File.open("tmp/links.txt","wb"){|w| w.write(linksstr) }
		File.open("tmp/db.txt","wb"){|w| w.write(data) }
		print "Baza linkow pobrana.\n"
		return links
	end
	
	# Download regions pages and descriptions
	def self.download_regions_descriptions
		$regionlist.each_line{|line|
			nobase = false
			x = line.split("^")
			name = x[2].gsub("\n","")
			print "Pobieranie regionu #{name}..\n"
			link = "http://www.altimetr.pl/"+name+".html"
			data = ""
			begin
				open(link){|s|
					# Save html region data
					data = s.read
					File.open("regions_html/#{name}.html","wb"){|w| w.write(data) }
				}
			rescue
				add_warning("404 error: #{link}\n")
			end
			description2 = data.split("<p class=\"opistrasy\">")[1].to_s
			description2 = description2.split("</p>")[0].to_s
			description = ""
			description2.each_line{|x|
				next if line.tr("\n\t\r\x20","")==""
				description << x.strip
			}
			description.strip!
			
			# Pobierz współrzędne podstawy
			podstawa = data.split("var podstawa = 	[")[1].to_s
			podstawa = podstawa.split("]")[0].to_s
			if podstawa == ","
				add_warning("Podstawa #{name} niezdefiniowana.")
				nobase = true # Ustaw brak podstawy
				zoom = 0
				center_x = 0
				center_y = 0
			else
				podstawa = podstawa.split(",")
				zoom = podstawa[0]
				center_x = podstawa[1]
				center_y = podstawa[2]
			end
			
			description = clear_html(description)
			result = "description=#{description}\nzoom=#{zoom}\ncenter_x=#{center_x}\ncenter_y=#{center_y}"
			
			fname = "regions/#{name}.txt"
			if nobase == true
				# Nie nadpisuj danych o podstawie
				File.open("regions/#{name}.txt","wb"){|w|w.write(result) } unless File.exist?(fname)
				add_warning("Podstawa #{name} musi byc zdefiniowana recznie")
			else
				File.open("regions/#{name}.txt","wb"){|w|w.write(result) }
			end
		}
	end 
	
	# Download regions list
	def self.download_regions
		# Read from file
		data = File.read("tmp/db_html.txt")
		# Remove mess before db
		data = data.split("<div id=\"baza\" class=\"baza\">")[1]
		# Remove mess after db
		data = data.split("</div>")[0]
		# Remove blank signs
		#data = data.tr("\x20\t","")
		data = data.tr("\t","")
		#data = data.tr("\x20","^SPACE^")
		# Create link database
		regions = []
		# Each line - check that line is region link
		data.each_line{|line|
			next if line.tr("\x20","") == "" # Remove blank lines
			next if !line.include?("<h4>") # Only region links
			# Remove all that isn't a link
			line2 = line.split("<h4>")[1] # Remove link start
			line2 = line2.to_s
			link = line2.split("\">")[0]
			link = link.split("<a href=\"")[1]
			line2 = line2.split("\">")[1]
			line2 = line2.to_s.gsub("^SPACE^","\x20")
			# Remove link at end
			line2 = line2.to_s.split("</a></h4>")[0]
			line2.gsub!("\n","")
			link.gsub!("\n","")
			link.gsub!("http://www.altimetr.pl/","")
			link.gsub!(".html","")
			regions << "#{line2}^#{link}"
		}
		regions.compact!
		# DEBUG: save to file
		regionsstr = ""
		count = 0
		regions.each{|x|
			count += 1
			regionsstr << "#{count}^#{x}\n" 
		}
		File.open("tmp/regions.txt","wb"){|w| w.write(regionsstr) }
		print "Baza regionow pobrana.\n"
		# Zaktualizuj dane regionów
		$regionlist = File.read("tmp/regions.txt") if File.exist?("tmp/regions.txt")
		$regionfixlist = File.read("tmp/regions_fix.txt") if File.exist?("tmp/regions.txt")
		return regions
	end
	
	def self.download_climb(link, id)
		# Generate filename
		htmlfilename = link.gsub("http://","").gsub("www.altimetr.pl/","").gsub("/","")
		filename = htmlfilename.gsub(".html",".txt")
		if $localbase == false
			# Dane z internetu
			serverclimbdata = lambda { open(link){|s| return s.read } }.call
			# Save climb html
			File.open("climbs_html/#{htmlfilename}","wb"){|w| w.write(serverclimbdata) }
		else
			# Dane lokalne
			serverclimbdata = File.read("climbs_html/#{htmlfilename}")
		end
		# Pobierz nachylenie
		slope = serverclimbdata.split("<div id=\"high\" class=\"pomiar\">")[1]
		slope = slope.to_s.split("</div>")[0]
		# Formatuj nachylenie
		slope.strip!
		
		# Pobierz nazwe podjazdu i sformatuj ją
		climbname = serverclimbdata.split("<div class=\"left-side\">")[1].to_s
		climbname = climbname.split("<h1>")[1].to_s
		climbname = climbname.split("<br/>")[0]
		climbname.strip!
		
		# Pobierz dane nawierzchni i ruchu samochodowego
		
		surfacedata = serverclimbdata.split("<ul class=\"danetrasy\">")[1].to_s
		surfacedata = surfacedata.split("<li>")[3].to_s
		surfacedata = surfacedata.split("</li>")[0].to_s
		surfacedata.strip!
		
		# Pobierz opisy trasy
		desctable = serverclimbdata.split("<p class=\"opistrasy\">")
		description = ""
		size = desctable.size
		# size-1 => region data
		(size-2).times{|x|
			dsc = desctable[x+1]
			if dsc.include?("<br/>")
				dsc = dsc.split("<br/>")[0]
			else
				dsc = dsc.split("<br>")[0]
			end
			dsc = dsc.to_s.split("</p>")[0].to_s
			dsc = clear_html(dsc)
			dsc.strip!
			description = "#{description}\n\n#{dsc}"
		}
		description[0..1] = ""
		# Dodaj dane nawierzchni
		description.strip!
		description = "#{surfacedata}\n#{description}"
		
		# Pobierz region
		region = desctable[size-1]
		# Get clean region
		region = region.split("<p>")[1].to_s
		region = region.split("</a>")[0].to_s
		region = region.split("\">")[-1].to_s
		# Get region id
		region = get_region_id(region)
		
		# Pobierz komentarze
		comments = ""
		comments2 = serverclimbdata.split("<p class=\"komentarz\">")
		size = comments2.size
		(size-1).times{|x|
			com2 = comments2[x+1].to_s.split("</p>")[0]
			# Format HTML new lines to Ruby new lines
			com2 = com2.gsub("<br/>","\n").gsub("<br>","\n")
			comments << "#{com2}\n"
		}
		comments = comments.gsub("\n\x20","\n")
		comments[-1] = "" if comments[-1] == "\n"
		comments.strip!
		
		# Pobierz nazwę dodającego
		author = serverclimbdata.split("<p class=\"opistrasy\">")[1].to_s
		if author.include?("<br/>")
			author = author.split("<br/>")[1]
		elsif author.include?("<hr/>")
			author = author.split("<hr/>")[1]
		elsif author.include?("<hr>")
			author = author.split("<hr>")[1]
		else
			author = author.split("<br>")[1]
		end
		author = author.to_s.split("</p>")[0].to_s
		author.gsub!("Opis dodany przez użytkownika:","")
		author.gsub!("Opis dodany przez: ","")
		author.gsub!("Opis dodany przez:","")
		author = clear_html(author)
		author.strip!
		author = "???" if author == ""
		
		# Pobierz punkty
		points = serverclimbdata.split("<ul class=\"danetrasy\">")[1].to_s
		points = points.split("<li>Kategoria")[1].to_s
		# Fix dla podjazdów 6 kategorii
		if points.include?("bez kategorii")
			i = 1
		else
			i = 0
		end
		points = points.split("(")[1+i].to_s
		points = points.split(")")[0].to_s
		# Poprawka punktow
		points = points.gsub("ptk","")
		points = points.gsub("pkt","")
		points.strip!
		
		# Pobierz segment stravy
		if serverclimbdata.include?("strava.php")
			# Podjazd zawiera segment Stravy
			segmentid = serverclimbdata.split("strava.php?segment=")[1]
			segmentid = segmentid.split("'")[0]
		else
			segmentid = 0
		end
		
		# Start parsing location data
		
		# Remove mess before
		if serverclimbdata.include?("var mapaid = google.maps.MapTypeId.ROADMAP;")
			tosplit = "var mapaid = google.maps.MapTypeId.ROADMAP;"
		elsif serverclimbdata.include?("var mapaid = google.maps.MapTypeId.SATELLITE;")
			tosplit = "var mapaid = google.maps.MapTypeId.SATELLITE;"
		elsif serverclimbdata.include?("var mapaid = google.maps.MapTypeId.HYBRID;")
			tosplit = "var mapaid = google.maps.MapTypeId.HYBRID;"
		elsif serverclimbdata.include?("var mapaid = google.maps.MapTypeId.TERRAIN;")
			tosplit = "var mapaid = google.maps.MapTypeId.TERRAIN;"
		end
		climbdata = serverclimbdata.to_s.split(tosplit)[1]
		# Remove mess after
		climbdata = climbdata.to_s.split("</script>")[0]
		# Remove blank spaces
		climbdata.tr!("\t\x20","")
		# Remove JS var construction and blank space
		climbdata = climbdata.gsub("var","").gsub(";","")
		climbdata.strip!
		result = "name=#{climbname}\nslope=#{slope}\npoints=#{points}\nregion=#{region}\ndescription=#{description}\ncomments=#{comments}\nauthor=#{author}\nsegment=#{segmentid}\n#{climbdata}"
		result.gsub!(";","")
		# Save to file
		File.open("climbs/#{filename}","wb"){|w| w.write(result) }
		filename = filename.gsub(".txt","")
		$climbhashes << "#{id}^#{filename}\n"
		print "Podjazd #{filename} pobrany.\n"
	end
	
	def self.download_all_climbs
		links = self.download_links
		lsize = links.size
		lcount = 0
		links.each{|x|
			lcount += 1
			print "#{lcount}/#{lsize} "
			self.download_climb(x, lcount) #rescue lambda{ print "Nie udalo sie pobrac podjazdu #{x}\n" }.call
		}
		print "Zapisywanie climb hashes..\n"
		File.open("tmp/climbhashes.txt","wb"){|w| w.write($climbhashes) }
	end
	
	def self.generate_climb_hashes
		files = []
		Dir.foreach("climbs"){|x|
			next if x == "."
			next if x == ".."
			files << x
		}
		$climbhashes = ""
		count = 0
		files.each{|x|
			name = x.gsub("climbs/","").gsub(".txt","")
			count += 1
			shahash = Digest::SHA1.hexdigest(x)
			$climbhashes << "#{count}^#{name}\n"
		}
		File.open("tmp/climbhashes.txt","wb"){|w| w.write($climbhashes) }
	end
	
	def self.download_climbs_from_links(file)
		data = File.read(file)
		count = 0
		data.each_line{|x|
			x = x.strip
			next if x== "." || x== ".."
			count += 1
			Altimetr.download_climb(x, count)
		}
	end
	
end

begin
	if(ARGV.size == 0)
		$localbase = false
		Altimetr.download_orig_db
		Altimetr.download_links
		Altimetr.download_regions
		Altimetr.download_regions_descriptions
		Altimetr.download_all_climbs
	elsif ARGV.size == 1 && ARGV[0] == "local"
		add_warning("Uzycie bazy lokalnej")
		$localbase = true
		Altimetr.download_all_climbs
	else
		# Debug only (Remove in future)
		#Altimetr.download_orig_db
		#Altimetr.download_links
		#Altimetr.download_regions
		#Altimetr.download_regions_descriptions
		#Altimetr.download_climb("http://www.altimetr.pl/podjazd-zalesi.html", 2509)
		#Altimetr.download_climb("http://www.altimetr.pl/podjazd-kralovahola.html", 2600)
		#Altimetr.download_climb("http://www.altimetr.pl/podjazd-gruszka.html", 2601)
		#Altimetr.download_climb("http://www.altimetr.pl/podjazd-gruszka2.html", 2602)
		#Altimetr.download_climb("http://www.altimetr.pl/podjazd-kazimierzdolny.html", 2603)
	end
ensure
	if $warnings==""
		print "\nBrak bledow.\n"
	else
		print "\nZnalezione bledy:\n"+$warnings
	end
end
