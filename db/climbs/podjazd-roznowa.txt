name=Rożnowa z Wieliczki
slope=2.8km 4.9%
points=73.3
region=41
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: średni.
Jednostajny podjazd z centrum Wieliczki, który popuszcza dopiero pod koniec. Pozbawiony mocniej nachylonych fragmentów.
comments=Dnia 2015-02-01 użytkownik Greek dodał:
Podjazd jest źle poprowadzony, powinien być Słowackiego aż do wojewódzkiej 964, a potem już tą właśnie drogą do Rożnowej. Tak jak jest teraz to nikt nie jeździ :)
Dnia 2015-02-02 Admin dodał:
Kolega Artu dokładnie tak podał trasę... 
Dnia 2015-02-02 użytkownik Greek dodał:
Ale ja nie mówię, że Ty źle dodałeś, tylko właśnie że kolega Artu źle podał trasę. Nikt tamtędy nie jeździ (wystarczy spojrzeć na strava heatmap) - a w bazie powinny być trasy najbardziej popularne - tak jak właśnie trasa główną 964. A bez sensu byłoby dodawać dwa niemal takie same podjazdy.
W dodatku część po ul. Zyblikiewicza jest objęta zakazem ruchu z wyłączeniem mieszkańców. Wiadomo, że rowerem się tak czasem jeździ, ale nie powinno być to w bazie.
Dnia 2015-02-02 Admin dodał:
no okej, kolega Artu wybaczy (lub nie) -) Zmieniam na tę popularniejszą trasę
author=Artu
segment=0
locations=[[1,49.98384114214494,20.0659956950376],[2,49.98246919492212,20.067580551320134],[3,49.9816820431852,20.06998336931281],[4,49.98005590683529,20.069437754062847],[5,49.97829921528031,20.06963310127196],[6,49.97654058618978,20.069779200748485],[7,49.97561268666054,20.06780301274307],[8,49.9753984905232,20.065085927276982],[9,49.97414501264453,20.06332383786173],[10,49.9726377213311,20.06190838227303],[11,49.97131290507357,20.060152066966793],[12,49.9696789264461,20.059211231290988],[13,49.96795756407653,20.058634277705437],]
podstawa=[[13,49.9753984905232,20.065085927276982],['start',49.9844000,20.0634000],['meta',49.9662400,20.0580300]]
daneflot=[[0.0,240.909],[0.2,246.952],[0.4,258.652],[0.6,269.871],[0.8,277.536],[1.0,287.400],[1.2,298.000],[1.4,312.097],[1.6,326.307],[1.8,336.794],[2.0,350.260],[2.2,358.003],[2.4,366.590],[2.6,374.550],[2.8,378.992],]