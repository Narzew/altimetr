name=Zagronki ze Zbludzy
slope=3.0km 8.1%
points=218.5
region=29
description=Nawierzchnia: szosa+szuter, dobry
Trasa ta różnie rysowana jest na mapach - szczególnie źle rysowany jest niebieski szlak w tym rejonie.
Mimo to istnieje ona w rzeczywistości w postaci widocznej na mapach Google.
Skręcamy obok przystanku - kierunek Kosarzyska/Zagorzyn - jest znak.
Początkowo trasa pnie się ku górze asfaltem, dalej przez asfaltowe serpentyny w lesie.
Dopiero na końcu asfalt zmienia się w bitą drogę - można spokojnie jechać szosówką.
Podjazd kończy się około ostatnich domów.
Dalej droga leśna prowadzi w dół do Kapliczki pod Modynią.
Na trasie niezwykłe widoki na Beskidy, Gorce, Pieniny, Tatry - wyśmienita panorama.
comments=
author=mx2047
segment=0
locations=[[1,49.59826302833705,20.35550578134837],[2,49.59927121001497,20.35768607554496],[3,49.60040143257002,20.359718848073385],[4,49.6016875775063,20.36146786115978],[5,49.60218817272215,20.361252338873214],[6,49.60246713778722,20.35908131538497],[7,49.60415428607803,20.359502857497432],[8,49.6050766122319,20.36128943536994],[9,49.60631964690118,20.360926635678425],[10,49.6076052650148,20.36232557949654],[11,49.60842542257677,20.36453965672763],[12,49.60948000006056,20.366477013222152],[13,49.60898361337224,20.368885703452747],[14,49.60955035506914,20.370530355042774],]
podstawa=[[14,49.6050766122319,20.36128943536994],['start',49.5981800,20.3534100],['meta',49.6109500,20.3720800]]
daneflot=[[0.0,545.846],[0.2,558.533],[0.4,579.674],[0.6,606.683],[0.8,622.757],[1.0,634.387],[1.2,650.122],[1.4,671.832],[1.6,695.181],[1.8,715.083],[2.0,728.81],[2.2,740.540],[2.4,751.92],[2.6,756.904],[2.8,769.928],[3.0,787.784],]