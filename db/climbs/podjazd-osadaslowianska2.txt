name=Osada Słowiańska od Łączek Kucharskich
slope=3.0km 4.9%
points=103.4
region=46
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Skrót dla chcących dostać się do Dębicy szybciej, alternatywa dla podjazdu w Okoninie. Krótki, ale ciekawy, nachylenie niemałe, potrafi zmęczyć, a to co najbardziej w nim lubię to że wąski, świetny asfalt, ruch mały i malowniczy. Najgorszy jest początek, dość stromo ale da się usiedzieć w siodle, potem lżej, ale dalej czuć że jedziemy pod górę. Parę niebezpiecznych zakrętów, na których należy uważać. Na szczycie można skręcić w lewo a później w prawo, zeby odwiedzić opisany na przeze mnie podjazd koło grodu lub w prawo i tam też jest zjazd do Stobiernej.
comments=Dnia 2015-01-02 użytkownik thedk63 dodał:
w opisie tego podjazdu powinno być : Osada Słowianska - od Łączek Kucharskch
Dnia 2015-01-02 Admin dodał:
Chyba masz racje. Poprawiłem
Dnia 2015-02-17 użytkownik thedk63 dodał:
Osada Słowiańska jest kilkaset metrów dalej na niższej wysokości.Dlatego też zmienił bym jeszcze opis na Stobierna od od Łączek Kucharskich.
author=TomekJac
segment=0
locations=[[1,50.02568056350246,21.54095123584534],[2,50.02631403787941,21.538339397435266],[3,50.02693196322165,21.535680551908627],[4,50.02778307312183,21.533347718248706],[5,50.02749174898036,21.530776769803992],[6,50.02802669555491,21.528077935386477],[7,50.02789951469281,21.525509368372354],[8,50.02825265917278,21.522871928732684],[9,50.0287198875758,21.520150611146164],[10,50.02924385607124,21.51744050166451],[11,50.02958910800155,21.5146823304367],[12,50.02938201861802,21.511894863002],[13,50.02820191302983,21.509934945270516],[14,50.02668413160932,21.508547628368433],]
podstawa=[[14,50.02825265917278,21.522871928732684],['start',50.0255100,21.5437200],['meta',50.0252200,21.5070800]]
daneflot=[[0.0,219.071],[0.2,226.941],[0.4,246.533],[0.6,265.176],[0.8,279.548],[1.0,299.116],[1.2,318.420],[1.4,324.000],[1.6,328.512],[1.8,337.630],[2.0,342.820],[2.2,346.453],[2.4,352.457],[2.6,353.680],[2.8,363.302],[3.0,365.161],]