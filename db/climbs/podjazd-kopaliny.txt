name=Kopaliny z Gumnisk
slope=1.7km 7.1%
points=113.6
region=46
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Podjazd od strony Gumnisk. Po skręcie w lewo (jadąc od strony Dębicy), jedziemy między domami i łąkami, a następnie skręcamy w lewo znów i ti zaczyna się właściwa część. Nachylenie jest spore, jest moment, gdzie na moje doświadczone podjazdami oko jest napewno więcej niż 17 %. Są momenty dla wytchnienia, jednak jak na te okolice, robi wrażenie mimo że nie jest długi Do tego wąsko więc trzeba uważać na zjeżdżających z góry "wariatów" (dwa niebezpieczne zakręty). Podjazd kończy się w tzw. Kopalinach, na skrzyżowaniu, którym mając górski rower można dojechać do Stasiówki, a w lewo i potem w prawo do Dębicy przez ulice Tetmajera i Polną (również ciekawe podjazdy).
comments=
author=TomekJac
segment=0
locations=[[1,50.00297478072668,21.410276275854017],[2,50.00364239344961,21.41114533920495],[3,50.0043305961382,21.41187863878008],[4,50.00502879742405,21.412134288694233],[5,50.00587657227999,21.412336647423217],[6,50.00666822838468,21.412908227827074],[7,50.00748035747087,21.41334416678285],[8,50.0082036755301,21.414085184903342],[9,50.00895789299918,21.41475716501384],[10,50.00974403244518,21.415328064842925],[11,50.01004937688351,21.416573900977937],[12,50.01072941521696,21.417355824352057],[13,50.01159322734513,21.417558208721402],[14,50.01238051814876,21.418092430237948],[15,50.01313776523151,21.418771278061513],[16,50.01390325912593,21.41942751581155],]
podstawa=[[14,50.00895789299918,21.41475716501384],['start',50.0024300,21.4092500],['meta',50.0146900,21.4200200]]
daneflot=[[0.0,241.469],[0.1,244.746],[0.2,250.415],[0.3,253.750],[0.4,256.743],[0.5,266.050],[0.6,275.989],[0.7,287.573],[0.8,299.560],[0.9,309.912],[1.0,317.105],[1.1,318.107],[1.2,329.840],[1.3,343.498],[1.4,353.276],[1.5,357.853],[1.6,358.884],[1.7,361.372],]