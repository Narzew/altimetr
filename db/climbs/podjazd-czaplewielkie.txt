name=Czaple Wielkie
slope=0.7km 8.3%
points=58
region=65
description=Nawierzchnia: szosa, stan bardzo dobry  . Ruch samochodowy: niewielki  .
Podjazd do zabytkowego kościoła w Czaplach Wielkich. Szosa wąska, nawierzchna dobra, nachylenie spore i równomierne.
comments=
author=Stradovius
segment=0
locations=[[1,50.29680358666839,19.987990942252054],[2,50.29592356504619,19.987993391245254],[3,50.29509970006107,19.987511561719543],[4,50.29434744301747,19.986765679977793],[5,50.29358009818486,19.986059035094286],[6,50.29276839904979,19.98626914525039],]
podstawa=[[15,50.295,19.986765679977793],['start',50.2976400,19.9878500],['meta',50.2920100,19.9863300]]
daneflot=[[0.0,275.518],[0.1,277.669],[0.2,287.673],[0.3,297.200],[0.4,307.255],[0.5,320.228],[0.6,330.284],[0.7,333.681],]