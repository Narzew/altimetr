name=Kalwaria Pacławska od Fredropola
slope=2.3km 8.7%
points=221.8
region=48
description=Nawierzchnia: szosa. Ruch samochodowy: mały.
Jeden z najtrudniejszych podjazdów na Pogórzu Przemyskim.
comments=Dnia 2015-02-15 użytkownik parurex dodał:
Bardzo ciężki podjazd min przez to, że na samym końcu ok 300m przed końcem, czeka jedna z największych stromizn. Podjazd z serii wyciskaczy łez 
Dnia 2015-06-27 użytkownik parurex dodał:
Najbardziej stromy podjazd pod Kalwarię. Na początku przed lasem jest mała rozgrzewka, ale po zakręcie w lewo w lesie zaczyna się cały czas trzymające spore nachylenie. Na deser najmocniejszy odcinek jest na samym końcu, gdy każdy ma już dość. Ruch jest dość uwarunkowany, w dni powszednie bardzo mało osób jeździ tą drogą. Jednak podczas niedziel i świąt trzeba uważać, gdyż ruch jest wtedy dość wzmożony. Przy zjeździe trzeba bardzo uważać, zakręty co 100m powodują że hamulce na dole są rozgrzane do czerwoności
author=dlugi73
segment=0
locations=[[1,49.64154900756605,22.720596577829497],[2,49.64092800611179,22.71957318176385],[3,49.64030699563769,22.718549811802063],[4,49.63968597614414,22.717526467943458],[5,49.63906494763158,22.71650315018701],[6,49.63839036912385,22.71561389735598],[7,49.63763259516946,22.714973811619075],[8,49.63675262659623,22.71487450066377],[9,49.63611589558331,22.71393444328612],[10,49.63522086551524,22.71378152243676],[11,49.63449105377322,22.714531403466708],[12,49.6337297409766,22.71498117311785],[13,49.63310163723312,22.714121283738223],[14,49.63308565887026,22.7128042116924],[15,49.63221242905081,22.712518511983603],[16,49.63147227607065,22.711868692823145],[17,49.63223455590244,22.711166509215218],[18,49.63302598085703,22.710576329948026],[19,49.63323749272936,22.709258696432016],[20,49.6331956618348,22.707883074796428],[21,49.63279944102808,22.706625233373984],[22,49.63230413550183,22.70569957539601],]
podstawa=[[14,49.636,22.71498117311785],['start',49.6421700,22.7216200],['meta',49.6315000,22.7057700]]
daneflot=[[0.0,248.857],[0.1,251.496],[0.2,256.923],[0.3,265.200],[0.4,277.600],[0.5,285.515],[0.6,291.476],[0.7,308.477],[0.8,322.528],[0.9,334.133],[1.0,344.099],[1.1,348.807],[1.2,351.803],[1.3,357.423],[1.4,373.438],[1.5,384.445],[1.6,396.088],[1.7,408.749],[1.8,416.594],[1.9,429.183],[2.0,439.640],[2.1,447.824],[2.2,446.683],[2.3,449.186],]