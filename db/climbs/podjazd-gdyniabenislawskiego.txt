name=Gdynia (Benisławskiego)
slope=0.8km 6.5%
points=34.8
region=90
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: duży.
Dość ruchliwa trasa, którą najlepiej pokonywać poza godzinami szczytu, inaczej wjazd na "rondo" może wiązać się z małymi kłopotami.
comments=
author=m@roni
segment=0
locations=[[1,54.54728372812763,18.520397058346134],[2,54.54806473092238,18.521261307452846],[3,54.54890254470329,18.521769048009332],[4,54.54982389190391,18.521850555557194],[5,54.55075094679683,18.521917977240946],[6,54.55166409723113,18.522167048314827],[7,54.55253538707278,18.522713434152365],]
podstawa=[[15,54.55075094679683,18.521917977240946],['start',54.5464900,18.5195700],['meta',54.5533500,18.5234800]]
daneflot=[[0.0,7.415],[0.1,14.002],[0.2,19.592],[0.3,24.702],[0.4,30.659],[0.5,38.446],[0.6,45.095],[0.7,52.706],[0.8,59.732],]