name=Przełęcz Biadoszowska z Inwałdu
slope=1.2km 8.2%
points=99.8
region=21
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Poważniejsza niż by się mogło wydawać na pierwszy rzut oka. Wysokość przełączki i łączne przewyższenie podjazdu mogą wzbudzać politowanie jak na standardy beskidzkie, ale to bardzo ciekawy podjazd. Rozpoczynamy na rozjeździe bocznych dróg (Wiejska i Dworska) i wspinamy się ulicą Zagórnicką wąską szosą o znakomitej nawierzchni. Jest bardzo klimatycznie i całkiem stromo. Na samej przełęczy kapliczka i znaki szlaku turystycznego.
comments=
author=Stradovius
segment=0
locations=[[1,49.85583258541026,19.378774157469024],[2,49.8549156240492,19.378857783980266],[3,49.85400151787144,19.378866076443842],[4,49.85315522720934,19.379416524612452],[5,49.852348375986,19.3800904506636],[6,49.85148629911829,19.38054685049815],[7,49.85070805341507,19.381254181600752],[8,49.84997229432566,19.382029399926523],[9,49.84914119304582,19.382363128517454],[10,49.84824103420129,19.382077980929807],[11,49.84732970963974,19.381959284662003],]
podstawa=[[14,49.85070805341507,19.381254181600752],['start',49.8565200,19.3792900],['meta',49.8464600,19.3815000]]
daneflot=[[0.0,339.087],[0.1,342.088],[0.2,345.536],[0.3,350.353],[0.4,356.108],[0.5,366.044],[0.6,378.149],[0.7,388.097],[0.8,397.067],[0.9,409.366],[1.0,419.921],[1.1,435.033],[1.2,437.364],]