name=Czerna II
slope=0.9km 7.4%
points=54.5
region=62
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Wąska asfaltowa trasa początkowo między domami potem przez las. Droga przelotowa w całości asfaltowa. W rejonie skrzyżowania z drogą do Paczółtowic łączymy się z szeroką drogą. Trasa spokojna, leśna atrakcyjna.
comments=Dnia 2015-05-07 użytkownik xtnt dodał:
VDO M4 wykrył tu chwilowe 15%, ale sporo 12-13% ogólnie fajnie stromo, zjazd jest zabójczy, a okolica wg mnie bardzo atrakcyjna. Wyjątkowy podjazd. Polecam.
Dnia 2015-05-08 użytkownik xtnt dodał:
Dodam, że dla mnie podjazd ten zaczyna się tak:
https://goo.gl/maps/MiJZx
W ten sposób podjazd ma o 80m przewyższenia więcej, początek jest niczym (2-3%), ale w całości zmienia to postać rzeczy. Dla ludzi trenujących przewyższenie będzie to miało znaczenie. Poza tym taka jest większość podjazdów, skromny długi początek (Np. Hala Boracza) stromy koniec. Albo podjazd obok Nowa Góra (Paryż) od Woli Filipowskiej jest dokładnie taki sam, łagodny początek, stromy koniec. Ale to już zależy od metody dodawania, w pierwszym wypadku wychodzi krótki podjazd, duży % średniej, w drugim większe przewyższenie, długi podjazd, mniejszy % średniej.
author=szymek
segment=0
locations=[[1,50.17462805683436,19.617770043523592],[2,50.17505350014692,19.618965611249678],[3,50.17571903268524,19.619831683598704],[4,50.17656849119005,19.620348006320455],[5,50.17736099850327,19.62105093609307],[6,50.17747029087576,19.622115259944167],[7,50.17723848924532,19.623463561139943],[8,50.17728788771432,19.624882544266143],]
podstawa=[[15,50.17736099850327,19.62105093609307],['start',50.1737800,19.6175000],['meta',50.1770800,19.6261400]]
daneflot=[[0.0,352.124],[0.1,357.517],[0.2,367.677],[0.3,377.720],[0.4,382.022],[0.5,391.376],[0.6,399.695],[0.7,409.146],[0.8,413.759],[0.9,418.719],]