name=Lublin (Kosmowskiej)
slope=0.6km 4.3%
points=12.1
region=82
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: duży.
Podjazd łączący Czechów Południowy z Czechowem Północnym, niezbyt stromy, aczkolwiek w Lublinie ciężko znaleźć coś stromego. Po prawej stronie jest chodnik, aczkolwiek nie ma możliwości przejechania nim całości podjazdu. Uwaga na autobusy, nie jest przyjemne spotkanie z autobusem na tym podjeździe, gdyż nie ma zbytnio gdzie zjechać.
comments=
author=Narzew
segment=0
locations=[[1,51.25846805642118,22.53805932449677],[2,51.25930720236583,22.538261300809154],[3,51.26010359755588,22.538663755972834],[4,51.26057599563433,22.53978440460139],[5,51.26102066097408,22.540939069163983],]
podstawa=[[15,51.26057599563433,22.53978440460139],['start',51.2576500,22.5379700],['meta',51.2616300,22.5417100]]
daneflot=[[0.0,181.941],[0.1,186.489],[0.2,191.440],[0.3,197.620],[0.4,201.513],[0.5,205.439],[0.6,208.041],]