name=Cisie
slope=1.0km 7.7%
points=98.6
region=65
description=Nawierzchnia: szosa, stan dobry   . Ruch samochodowy: incydentalny  .
Podjazd z miejscowości Cisie do Antolki ze swoimi dwoma ostrymi serpentynami dobrze oddaje charakter Wyżyny Miechowskiej. Nietypowe jest w nim tylkoto, że przebiega przez las (rzadkość na obszarze z tak dobrymi glebami).
comments=
author=Stradovius
segment=0
locations=[[1,50.41050085036662,20.09043107321986],[2,50.41000260086957,20.091556940167266],[3,50.4092165602294,20.092254031938637],[4,50.40924635787003,20.091227387543654],[5,50.40906751516012,20.090402337141427],[6,50.40826280267008,20.091044052682605],[7,50.40750671460847,20.091821204041025],[8,50.40691911625802,20.092896873793848],[9,50.40634771040001,20.09399343538962],]
podstawa=[[15,50.40826280267008,20.091044052682605],['start',50.4105900,20.0890200],['meta',50.4056200,20.0948300]]
daneflot=[[0.0,279.963],[0.1,290.402],[0.2,304.415],[0.3,316.167],[0.4,307.474],[0.5,306.186],[0.6,319.310],[0.7,327.679],[0.8,338.128],[0.9,349.737],[1.0,357.327],]