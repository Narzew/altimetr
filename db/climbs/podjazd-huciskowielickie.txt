name=Hucisko (Pogórze Wielickie)
slope=1.2km 7.8%
points=82.2
region=41
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Krótki, lecz całkiem stromy podjazd w Hucisku. Po skręcie z głównej drogi w trochę węższy asfalt nachylenie stopniowo wzrasta, następnie utrzymuje się w okolicy 10%. Po wjeździe do lasu skręcamy w prawo, a droga się wypłaszcza. Na koniec jeszcze krótki stromy fragment na najwyższy punkt w lesie obok Huciska.
comments=
author=cymek
segment=0
locations=[[1,49.91780021340708,20.135503740320473],[2,49.91715564960996,20.13464163378694],[3,49.9165360977164,20.133656988099574],[4,49.91591137280061,20.132693111661524],[5,49.91561502531193,20.13139107920108],[6,49.91533988706428,20.130077060389453],[7,49.91509876125009,20.128749823672592],[8,49.91493340214267,20.12739146936417],[9,49.91506977155824,20.126264347214487],[10,49.91578155392554,20.12545951435027],[11,49.91640921292379,20.124479229410554],]
podstawa=[[15,49.916,20.128749823672592],['start',49.9185800,20.1356700],['meta',49.9170300,20.1234900]]
daneflot=[[0.0,253.812],[0.1,257.744],[0.2,267.323],[0.3,277.771],[0.4,286.371],[0.5,296.875],[0.6,308.367],[0.7,317.837],[0.8,326.513],[0.9,330.176],[1.0,335.143],[1.1,340.442],[1.2,348.055],]