name=Krzeszów
slope=0.8km 6.5%
points=44.5
region=68
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Na początku podjazdu brak asfaltu w dwóch miejscach na odcinku ok. 1-2 m (szuter). Przy podjeździe to nie przeszkadza ale trzeba uważać na zjezdzie. W niedzielę, w godzinach mszy wzmożony ruch pieszych.
comments=
author=rjarosz
segment=0
locations=[[1,50.40436487746546,22.343022241534527],[2,50.4049514655502,22.344170904763587],[3,50.40526725748123,22.345549697556635],[4,50.40576682798909,22.346793458984394],[5,50.40637581087842,22.347926206346415],[6,50.40675631946172,22.34927791619191],[7,50.40694473270673,22.35073076144738],]
podstawa=[[15,50.40637581087842,22.347926206346415],['start',50.4038500,22.3418100],['meta',50.4077000,22.3514400]]
daneflot=[[0.0,171.854],[0.1,174.583],[0.2,182.456],[0.3,190.902],[0.4,201.307],[0.5,213.394],[0.6,219.831],[0.7,222.611],[0.8,223.944],]