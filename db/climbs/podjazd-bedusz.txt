name=Będusz
slope=1.0km 4.8%
points=28.4
region=60
description=Nawierzchnia: szosa, stan dobry   . Ruch samochodowy: średni/duży  .
Najtrudniejszy podjazd Progu Woźnickiego. Przebiega drogą wojewódzką nr 793 o średnim lub dość dużym natężeniu ruchu (w zależności od dnia i godziny) i dobrej nawierzchni. Podjazd jest krótki i zaczyna się przy zespole szkół. W miejscu gdzie droga przecina zabudowania Będusza robi się dość stromo.
comments=Dnia 2015-02-05 użytkownik Stradovius dodał:
Spieszę donieść, że to Wyżyna Woźnicko-Wieluńska :)
Dnia 2015-02-21 użytkownik Stradovius dodał:
Uściślając - by xtnt był zadowolny - najwyższy punkt Wyżyny Woźnicko-Wieluńskiej i jej mezoregionu zwanego Progiem Woźnickim znajduje się ponad 8 km na zachód od tego miejsca (w linii prostej!), tuż obok wsi Markowice (386 m n.p.m). 
Podjazd nie jest wcale za krótki - jak na realia Progu Woźnickiego jest wręcz typowej długości. Po prawej są zabudowania dawnego folwarku i niezbyt zabytkowy XX-wieczny dwór o zwracającej uwagę wieży. Niestety obiekt jest brzydki, a na teren dawnego folwarku obowiązuje zakaz wstępu i wjazdu.
author=Stradovius
segment=0
locations=[[1,50.55532005430251,19.298503796890145],[2,50.55461010370843,19.297747616558354],[3,50.55390014821803,19.29699145900338],[4,50.55318489651882,19.296247784435536],[5,50.55246948553555,19.29550449637327],[6,50.55175406982143,19.294761230869995],[7,50.55102264370282,19.29406271606308],[8,50.55021747997437,19.293603023326227],[9,50.54943797081944,19.29304593862173],]
podstawa=[[15,50.55175406982143,19.294761230869995],['start',50.5560300,19.2992600],['meta',50.5487000,19.2923600]]
daneflot=[[0.0,307.335],[0.1,309.726],[0.2,312.448],[0.3,316.206],[0.4,319.177],[0.5,321.891],[0.6,326.634],[0.7,333.903],[0.8,343.391],[0.9,351.115],[1.0,355.064],]