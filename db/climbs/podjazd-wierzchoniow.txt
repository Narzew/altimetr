name=Wierzchoniów
slope=0.8km 7.6%
points=71.6
region=82
description=Nawierzchnia: szosa+beton, dobry
Droga malowniczym wąwozem po płytach ażurowych. Jedna trudniejsza ścianka, reszta przebiega raczej łagodnie. Podjazd jest odgrodzony z dwóch stron drogą polną (od Wierzchoniowa to jakieś 200m, od Lasu Stockiego kilka razy więcej, można przeprowadzić rower). Nawierzchnia na samym podjeździe przyjemna, płyty są w miarę równę, można nawet się solidnie rozpędzić przy zjeździe, ale trzeba uważać na kilku dość niebezpiecznych zakrętach. Zjeżdzając warto przyhamować kilka m przed końcem, gdyż wjazd z dużą prędkością na polną drogę do przyjemności nie należy. Ruch samochodowy praktycznie zerowy, droga jest wąska, miejscami ma około 3m szerokości, jedynie można spotkać tu pieszych, rowery i motocykle. Płyty ażurowe kończą się kilka m po osiągnieciu najwyższego punktu, dalej polna droga prowadzi do Lasu Stockiego.
comments=
author=Narzew
segment=9507556
locations=[[1,51.33791551988134,22.03648758581153],[2,51.33867852981634,22.035879750618278],[3,51.33942609089205,22.035179844959202],[4,51.34026845690411,22.034764130347185],[5,51.34106336413257,22.034242850220494],[6,51.34188235610846,22.034795968352],[7,51.3424677746239,22.03581443073881],]
podstawa=[[15,51.34106336413257,22.034242850220494],['start',51.3370600,22.0368700],['meta',51.3428700,22.0370800]]
daneflot=[[0.0,133.982],[0.1,138.634],[0.2,156.030],[0.3,171.004],[0.4,181.685],[0.5,186.901],[0.6,188.533],[0.7,192.738],[0.8,194.959],]