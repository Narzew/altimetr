name=Życzanów
slope=2.4km 13.6%
points=490.8
region=32
description=Nawierzchnia: szosa, stan średni. Ruch samochodowy: mały.
Stromy podjazd po małej miejscowości.
comments=Dnia 2014-08-17 użytkownik mx2047 dodał:
Podjazd rzeczywiście bardzo stromy - na jednym zakręcie klinometr pokazał mi ponad 40% nachylenia. Asfalt jest w niezbyt dobrym stanie ale do przeżycia - przeszkadza tylko żwirek podczas zjazdu. Przejechałem go szosówką i przeżyłem ale nie polecam tego powtarzać. Trzeba też uważać na nieuwiązane pieski.
Dnia 2015-05-15 użytkownik kr1s dodał:
na szosie nie polecam,zjazdy to wyzwanie,trzeba stanac by schlodzic obrecze.POdjazd bardzo sztywny
author=mx2047
segment=0
locations=[[1,49.50192298915767,20.67789471519086],[2,49.50265052821636,20.67990683100652],[3,49.5027829515564,20.681836627634652],[4,49.5025300001975,20.684577629886462],[5,49.50349168944973,20.684178037290394],[6,49.50309193778468,20.686856913007887],[7,49.50342827419253,20.689587034835654],[8,49.50308845337751,20.692196085683122],[9,49.50294149588188,20.69497375448975],[10,49.50342668926177,20.697665191527904],[11,49.50402007293229,20.700187210008494],]
podstawa=[[14,49.50342827419253,20.689587034835654],['start',49.5002500,20.6768600],['meta',49.5039800,20.7028900]]
daneflot=[[0.0,335.031],[0.2,353.857],[0.4,384.075],[0.6,408.370],[0.8,443.189],[1.0,460.861],[1.2,486.181],[1.4,535.616],[1.6,568.405],[1.8,600.753],[2.0,617.488],[2.2,639.219],[2.4,660.971],]