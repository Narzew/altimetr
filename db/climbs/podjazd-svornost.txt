name=Chata Svornost  (Czechy)
slope=0.9km 14.7%
points=238.8
region=14
description=Nawierzchnia: szosa, stan średni. Ruch samochodowy: minimalny.
Bardzo sztywny podjazd asfaltowy do pensjonatu Svornost. Na asfalcie wymalowane są znaczniki dystansu (co 10 metrów) i aktualny rekord podjazdu (3 min. 50 sek.). Na początku 10% i sukcesywnie rośnie pod 30% na odcinku w lesie.
comments=
author=???
segment=0
locations=[[1,50.20560592544059,17.23392564266419],[2,50.20472159710037,17.233764033580997],[3,50.20383422995467,17.23373105723317],[4,50.20297223919034,17.233383757582487],[5,50.20213911102317,17.232890616774853],[6,50.20128418372607,17.232690974164257],[7,50.2006108275358,17.233560500416388],[8,50.20043813466109,17.234891344009952],]
podstawa=[[15,50.204,17.232890616774853],['start',50.2064200,17.2333900],['meta',50.2008100,17.2361300]]
daneflot=[[0.0,525.203],[0.1,531.508],[0.2,544.797],[0.3,558.343],[0.4,569.537],[0.5,591.783],[0.6,614.767],[0.7,636.733],[0.8,655.494],[0.9,657.651],]