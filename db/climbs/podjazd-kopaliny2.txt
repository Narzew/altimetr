name=Kopaliny z Dębicy (ul. Polna)
slope=2.6km 5.9%
points=111.6
region=46
description=Nawierzchnia: szosa, stan dobry, nie licząc tej małej przerwy w asfalcie. Ruch samochodowy: minimalny.
Jedna z najbardziej malowniczo położonych ulic w Dębicy. Bardzo wąsko, przez moment nawet jedzie się w małym wąwozie. Im Wyżej tym ciekawiej, najpierw towarzyszą nam zabudowania, później las a na szczycie ładny widok na miasto (ze skrzyżowania z Tetmajera - też ciekawy podjazd). Kawałek dalej kończy się na chwilę asfalt, ale dla upartego szosowca to nie problem, da radę przejechać. Ze szczytu można zjechać szybkim zjazdem do Gumnisk.
comments=
author=TomekJac
segment=0
locations=[[1,50.03462833343674,21.413398583270236],[2,50.03280967303551,21.41357475039763],[3,50.03109534689149,21.4145593207445],[4,50.02947680962159,21.4159652986516],[5,50.02801925973582,21.41776296316175],[6,50.02648485078429,21.419343911694114],[7,50.02535934317228,21.421536642176534],[8,50.02397782343802,21.42342165919979],[9,50.02279506228177,21.42535658875181],[10,50.02095218567226,21.425327126918774],[11,50.01930171726099,21.424351592574567],[12,50.01761032061971,21.423323426768206],]
podstawa=[[13,50.02535934317228,21.421536642176534],['start',50.0363000,21.4140900],['meta',50.0158600,21.4226200]]
daneflot=[[0.0,218.074],[0.2,221.104],[0.4,236.417],[0.6,250.342],[0.8,255.303],[1.0,266.284],[1.2,279.780],[1.4,300.821],[1.6,319.925],[1.8,331.998],[2.0,342.853],[2.2,346.928],[2.4,365.370],[2.6,371.536],]