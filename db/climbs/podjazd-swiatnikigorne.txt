name=Świątniki Górne od Rzeszotary
slope=1.0km 7.7%
points=67.9
region=41
description=Nawierzchnia: szosa, stan średni. Ruch samochodowy: mały.
Podjazd do centrum Świątnik Górnych. Początek może sprawiać trudności, czeka nas tam stroma część podjazdu o złym stanie nawierzchni. Później jest już tylko lepiej i możemy cieszyć się jazdą.
comments=Dnia 2014-08-14 użytkownik Pączek dodał:
Na pierwszych metrach stromizny położono nową warstwę asfaltu. Teraz stan nawierzchni na całej długości podjazdu oceniam na bardzo dobry.
author=Pączek
segment=0
locations=[[1,49.94187836825304,19.950358291363273],[2,49.94132877039409,19.949263962005602],[3,49.9404418795899,19.949228418666962],[4,49.9395853100697,19.949738814165812],[5,49.93871872524355,19.95020272205568],[6,49.93790181338004,19.95084321850777],[7,49.93708296523289,19.951429700831795],[8,49.93648916419662,19.952489878533697],[9,49.93569952533719,19.953089940666814],]
podstawa=[[15,49.9396,19.95084321850777],['start',49.9423100,19.9516200],['meta',49.9348400,19.9534300]]
daneflot=[[0.0,303.711],[0.1,315.921],[0.2,328.126],[0.3,335.956],[0.4,341.946],[0.5,345.431],[0.6,350.169],[0.7,355.414],[0.8,366.203],[0.9,373.920],[1.0,380.779],]