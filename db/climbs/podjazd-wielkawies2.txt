name=Wielka Wieś z Prądnika Korzkiewskiego
slope=1.2km 8.3%
points=86.9
region=62
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Drugi najtrudniejszy podjazd w Wielkiej Wsi zaraz po trasie z Ujazdu. Trasa od strony północnej, mniej oświetlona. Podjazd zwany przeze mnie Botoją ponieważ nieopodal znajduje się stadnina koni "Botoja" W początkowej części trasę przecina popularny czerwony szlak orlich gniazd prowadzący do Ojcowskiego Parku Narodowego.
comments=
author=Tmina
segment=0
locations=[[1,50.15636328012419,19.870303173221828],[2,50.15622953008818,19.86891600066133],[3,50.15617258266179,19.8675084719722],[4,50.15586665062212,19.866186606985593],[5,50.15575419650464,19.864852639042738],[6,50.15622944958198,19.863744861729174],[7,50.15581977760215,19.862495253479665],[8,50.15526433337516,19.861431681482713],[9,50.15454023863018,19.860587446847376],[10,50.15390208999172,19.859602106041734],[11,50.15353598146646,19.85831109195351],]
podstawa=[[14,50.15581977760215,19.862495253479665],['start',50.1565900,19.8716700],['meta',50.1531700,19.8570200]]
daneflot=[[0.0,268.134],[0.1,275.724],[0.2,284.347],[0.3,289.960],[0.4,299.444],[0.5,310.381],[0.6,318.509],[0.7,329.896],[0.8,340.152],[0.9,348.372],[1.0,356.660],[1.1,363.334],[1.2,367.101],]