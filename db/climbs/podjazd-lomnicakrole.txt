name=Łomnica Zdrój (Króle) z Piwnicznej
slope=4.4km 7.0%
points=343.9
region=32
description=Nawierzchnia: szosa+beton, dobry
Z ronda na drodze 87 skręcamy do Łomnicy Zdrój i jedziemy asfaltową drogą o małym natężeniu ruchu w górę wsi po około 2 km skręcamy  w lewo w betonową drogę w kierunku przysiółka Króle i jedziemy do końca betonowych płyt. Dalej można jechać góralem szlakiem rowerowym do Wierchomli lub do schroniska na Łabowskiej hali /miejscami niestety prowadząc rower/.
comments=Dnia 2015-05-13 użytkownik airstreeem20 dodał:
Powyżej od klasztoru redemptorystów, który obecnie jest obiektem wypoczynkowym - przepiękna trasa hippiczna, więc na drodze możemy napotkać urocze amazonki.
Dnia 2015-05-15 użytkownik Kuba dodał:
Po 2km skręcamy w PRAWO, a nie w lewo ! / Drogowskaz niebieskie napisy na białym tle/. Po lewej stronie po drugiej stronie potoku jest żródło wody mineralnej, można napełnić bidon.
author=Kuba
segment=0
locations=[[1,49.42066230748177,20.72703402267109],[2,49.42191649986746,20.72902204229274],[3,49.42218619839374,20.73166010489149],[4,49.42203385507909,20.734422703318046],[5,49.42266254101144,20.73700370802635],[6,49.42383066001285,20.739103063638254],[7,49.42530934224651,20.740654980749923],[8,49.42682631594761,20.742097369040607],[9,49.42809391748503,20.74347050396898],[10,49.42977584800672,20.74381684925845],[11,49.43002224356213,20.746507934135934],[12,49.4296927365685,20.749241712234607],[13,49.43095085836696,20.75078439600634],[14,49.4325005282676,20.75212245694604],[15,49.43417230202093,20.753084414716],[16,49.43520456214068,20.75536046030504],[17,49.43611322253901,20.757395974151677],[18,49.43742480567342,20.759262497045256],[19,49.43879613584085,20.761039647785083],[20,49.4404831080507,20.762034677037036],[21,49.44196594644158,20.7635953669419],]
podstawa=[[13,49.4296927365685,20.749241712234607],['start',49.4200100,20.7259000],['meta',49.4433800,20.7652100]]
daneflot=[[0.0,386.356],[0.2,388.216],[0.4,394.638],[0.6,403.079],[0.8,407.245],[1.0,413.335],[1.2,415.841],[1.4,415.691],[1.6,421.709],[1.8,425.529],[2.0,435.214],[2.2,455.374],[2.4,485.604],[2.6,512.424],[2.8,542.945],[3.0,571.934],[3.2,600.941],[3.4,626.638],[3.6,654.232],[3.8,671.637],[4.0,676.701],[4.2,687.965],[4.4,693.761],]