name=Ciecierzyn
slope=1.5km 2.8%
points=17.1
region=82
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: bardzo duży.
Podjazd zaczyna się kilkaset metrów za mostem na Ciemiędze. Widać znak o rozpoczęciu podjazdu. Początek trudniejszy od końcówki. Ostrzegam iż podjazd jest bardzo niebezpieczny, z uwagi na olbrzymi ruch samochodowy, jak i skrzyżowanie z obwodnicą Lublina. Pomimo iż to jest jeden z trudniejszych podjazdów w okolicy Lublina, to radzę mimo wszystko go omijać z powodu olbrzymiego ruchu samochodowego, jak i faktu że po przejechaniu go  dość ciężko jest zawrócić. Dużo bezpieczniejszy jest pobliższy podjazd w Dysie, a przewyższenie jest podobne.
comments=
author=Narzew
segment=0
locations=[[1,51.31684356516199,22.60582999999997],[2,51.31604670381124,22.60519623289133],[3,51.31533992299698,22.60432259331651],[4,51.31465177620022,22.60341024458603],[5,51.31396362231555,22.602497923224405],[6,51.31327546134332,22.601585629230613],[7,51.31258535974211,22.600677114626365],[8,51.31189436980595,22.599770337536825],[9,51.31120337286802,22.598863587758842],[10,51.31053000000521,22.59794352054928],[11,51.30987905004758,22.596980206998637],[12,51.30936745674004,22.59626965287464],[13,51.30867378236177,22.59536893102063],[14,51.30797008590508,22.594487605761742],]
podstawa=[[14,51.31189436980595,22.599770337536825],['start',51.3176800,22.6063200],['meta',51.3072600,22.5936200]]
daneflot=[[0.0,174.119],[0.1,177.109],[0.2,181.184],[0.3,187.210],[0.4,193.885],[0.5,198.599],[0.6,201.328],[0.7,203.378],[0.8,205.336],[0.9,206.656],[1.0,209.265],[1.1,208.247],[1.2,210.432],[1.3,213.437],[1.4,215.392],[1.5,215.882],]