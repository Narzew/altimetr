name=Góra św. Grzegorza od Jaworza Dolnego
slope=1.2km 13.3%
points=219.6
region=46
description=Nawierzchnia: szosa, stan średni. Ruch samochodowy: minimalny.
Najbardziej stromy podjazd pod kościół w Gorzejowej (Góra św. Grzegorza) Parę lat temu woda zerwała drogę i w miejscach naprawy jest kilka m nieprzyjemnego szutru.
comments=Dnia 2015-01-08 użytkownik thedk63 dodał:
Podjeżdżałem w 2014r.Max.nach. na liczniku pokazało 19%.
Dnia 2015-03-25 użytkownik przeminho dodał:
Ściana płaczu. Najgorszy kilometr jaki jechałem. Przy zjeździe jest naprawdę niebezpiecznie, ze względu na zakręty i wspomniany szuter
Dnia 2015-04-25 użytkownik TomekJac dodał:
Świetnie, szukałem miejsca z którego tam można podjechać, niedługo tam sie wybiorę :)
author=Topek
segment=0
locations=[[1,49.93538254288698,21.35062321472219],[2,49.93595444800646,21.35330029824206],[3,49.9360680896179,21.35609764173364],[4,49.93536665101423,21.35865653774772],[5,49.93443135941497,21.360633477299416],]
podstawa=[[15,49.93536665101423,21.35865653774772],['start',49.9355400,21.3478800],['meta',49.9344000,21.3634900]]
daneflot=[[0.0,227.222],[0.2,251.462],[0.4,283.987],[0.6,309.819],[0.8,343.237],[1.0,367.418],[1.2,386.759],]