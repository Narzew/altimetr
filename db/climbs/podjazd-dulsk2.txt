name=Dulsk od Ruże
slope=0.7km 5.3%
points=23.3
region=76
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: mały.
Jeden z najbardziej stromych podjazdów w okolicy, dobra nawierzchnia, mały ruch. Prawdopodobnie najszybszy zjazd w okolicy (możliwe ponad 70km/h). Ładny widok na dolinę na szczycie podjazdu.
comments=
author=ArturGabrych
segment=0
locations=[[1,53.04836305673712,19.140198065681602],[2,53.0487160999094,19.138926110525063],[3,53.04909061435108,19.137671298495434],[4,53.04953401360712,19.13648597321867],[5,53.05007265963895,19.135409124498665],[6,53.05061797949895,19.134341433847453],]
podstawa=[[15,53.04953401360712,19.13648597321867],['start',53.0480100,19.1414700],['meta',53.0511300,19.1332300]]
daneflot=[[0.0,74.849],[0.1,77.411],[0.2,81.439],[0.3,90.168],[0.4,98.887],[0.5,103.586],[0.6,108.928],[0.7,111.840],]