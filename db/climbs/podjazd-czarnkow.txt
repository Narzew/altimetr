name=Czarnków ul. Jesionowa
slope=0.5km 8.3%
points=40.5
region=72
description=Nawierzchnia: szosa, stan bardzo dobry. Ruch samochodowy: minimalny.
Kiedy nagle skręcisz w Czarnkowie w ulicę Jesionową, Twoim oczom ukaże się piękna ściana nowego gładkiego asfaltu ostro w górę. Po prostu trzeba zaliczyć ten krótki, ale sztywny podjazd.
comments=
author=Virenque
segment=0
locations=[[1,52.89862390355788,16.561604194549886],[2,52.89831481960465,16.56306745880147],[3,52.89825623755374,16.5646870587874],[4,52.89843005231801,16.566272811991894],]
podstawa=[[15,52.89825623755374,16.5646870587874],['start',52.8992300,16.5603300],['meta',52.8986600,16.5678500]]
daneflot=[[0.0,52.282],[0.1,58.463],[0.2,67.303],[0.3,79.031],[0.4,91.024],[0.5,93.908],]