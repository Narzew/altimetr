name=Kleszczów od Kochanowa
slope=1.0km 10.7%
points=120.5
region=64
description=Nawierzchnia: szosa, stan bardzo dobry. Ruch samochodowy: minimalny.
Jak widać, kilometr podjazdu 106 m. przewyższenia...solidne 10% :) ładny podjazd w lasku, trzeba zjechać w Kochanowie na trasie Kraków-Katowice na Kleszczów
comments=Dnia 2014-07-11 użytkownik Raku dodał:
Droga dojazdowa do Kochanowa dosyć uczęszczana mimo tego że jechałem z Krakowa od 630 w sobotę. Po wjeździe do lasku momentalnie chlodniej ale nie pomogło to w wspinaczce :):). Zjazd do Balic bardzo szybki (proponuję sprawdzić hamulce)
Dnia 2014-07-13 użytkownik Tmina dodał:
Dzisiaj wybrałem się na wycieczkę w celu porównaniu dwóch rzekomo najtrudniejszych podjazdów  w okolicach Krakowa po północnej stronie, Kleszczów- Od Kochanowa a później mój dobry znajomy Wielka Wieś- od Ujazdu. Oba mają 1km I ( śr. nachylenie 10,7% -148.4 ptk a drugi śr. nachylenie -11,2% - 134.9 ptk ) W sumie dziwna ta matematyka :P tym bardziej że dzisiaj empirycznie sprawdziłem że podjazd "Ujazd" jest dużo trudniejszy, bardzo stromy w połowie i na samym końcu co czuć w nogach. Kleszczów z Kochanowa trochę mnie dzisiaj rozczarował, jednolicie stromy ale bez szału.
Dnia 2014-07-13 Admin dodał:
Tu mały mój błąd, po przerobieniu podjazdu z mapy topograficznej nie zaktualizowałem punktów podjazdu. Podjazd powinien mieć 120,52 punktów i za chwilę to poprawię. Jeśli chodzi o punkty - zasady obliczania punktów są w zakładce O MAPIE.
author=Amator88>
segment=0
locations=[[1,50.11211367410522,19.7443498722389],[2,50.11038856919001,19.745515152545977],[3,50.10867981570866,19.74673477601391],[4,50.10693641830656,19.74776023814138],]
podstawa=[[15,50.111,19.74673477601391],['start',50.1139900,19.7441400],['meta',50.1062700,19.7505000]]
daneflot=[[0.0,248.337],[0.2,272.333],[0.4,300.932],[0.6,324.225],[0.8,338.322],[1.0,355.157],]