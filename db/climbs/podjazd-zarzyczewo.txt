name=Zarzyczewo
slope=0.5km 6.0%
points=23.1
region=76
description=Nawierzchnia: szosa, stan płyty betonowe. Ruch samochodowy: minimalny/średni.
Podjazd typu "tu i z powrotem". Krótki dość stromy, nierówne płyty betonowe dodatkowo utrudniają zwłaszcza gdy podjeżdżamy na szosie. Ruch nasilający się weekendy w okresie letnim.
comments=
author=Gabrych
segment=7366909
locations=[[1,52.66701131648612,19.166450743782434],[2,52.66693771876074,19.16492862048176],[3,52.66603235565397,19.16461182699095],[4,52.66557571581274,19.163951420979515],]
podstawa=[[15,52.66603235565397,19.16461182699095],['start',52.6669800,19.1680100],['meta',52.6657100,19.1625200]]
daneflot=[[0.0,60.023],[0.1,67.624],[0.2,75.329],[0.3,85.625],[0.4,87.319],[0.5,89.552],]