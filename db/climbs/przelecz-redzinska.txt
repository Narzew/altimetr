name=Przełęcz Rędzińska od  Wieściszowic
slope=3.2km 10.0%
points=361.3
region=5
description=Nawierzchnia: szosa, stan b. dobra. Ruch samochodowy: minimalny.
Bardzo stromo, ale nowy gładki asfalt od tego roku. Na kolarce idzie wyjechać. Z drugiej strony tj. od Pisarzowic również nowy asfalt.
comments=Dnia 2014-03-06 użytkownik dns dodał:
Zaczyna się szybko mimo początkowego dość sporego nachylenia. Należy się przygotować, że im dalej/wyżej tym stromiej co wykańcza w przypadku wcześniejszej nieznajomości profilu. Ostatnie serpentyny to bardzo dobrej jakości asfalt, aż do krzyża na przełęczy. Polecam ze względu na wysokość 800m i jakość asfaltu.
author=gavia2621
segment=4410074
locations=[[1,50.8370174638567,15.973006749213596],[2,50.83708909809697,15.970061218326464],[3,50.83628837866659,15.967391605109924],[4,50.83505143427818,15.965086914623612],[5,50.83427461725384,15.962398121882984],[6,50.8339190507177,15.95942598375484],[7,50.83345380450019,15.956499806979764],[8,50.83259141218534,15.953988358292008],[9,50.83370573562573,15.951618022932394],[10,50.83363822489879,15.948929717394549],[11,50.83317193743763,15.945997664758465],[12,50.8337441558466,15.943498545958164],[13,50.83322586351775,15.941251084196665],[14,50.83275663687648,15.939183318326286],[15,50.83086970064481,15.93922818134206],]
podstawa=[[14,50.833,15.955],['start',50.8375300,15.9759200],['meta',50.8290300,15.9386700]]
daneflot=[[0.0,483.385],[0.2,491.532],[0.4,501.861],[0.6,513.335],[0.8,531.276],[1.0,543.940],[1.2,561.632],[1.4,585.916],[1.6,616.484],[1.8,645.118],[2.0,667.636],[2.2,695.015],[2.4,718.378],[2.6,745.583],[2.8,774.548],[3.0,786.045],[3.2,802.474],]