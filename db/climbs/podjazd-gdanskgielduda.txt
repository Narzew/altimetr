name=Gdańsk (ul. Gen. Giełduda)
slope=0.6km 7.7%
points=37.7
region=90
description=Nawierzchnia: szosa+bruk, zmienny
Początkowo prowadzi po asfalcie, w końcówce bruk rodem z Paryż - Roubaix.
comments=
author=Lipa
segment=0
locations=[[1,54.36029985124852,18.639997017848486],[2,54.36023519819885,18.63839663167903],[3,54.36028128297465,18.636799626590914],[4,54.36000783253252,18.63529548190195],[5,54.35965407155739,18.63381316593552],]
podstawa=[[15,54.36000783253252,18.63529548190195],['start',54.3604100,18.6413700],['meta',54.3592800,18.6323500]]
daneflot=[[0.0,11.660],[0.1,16.987],[0.2,27.272],[0.3,36.876],[0.4,45.345],[0.5,52.675],[0.6,57.713],]