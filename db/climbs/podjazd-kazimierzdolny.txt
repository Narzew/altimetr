name=Kazimierz Dolny
slope=0.9km 8.8%
points=94.2
region=82
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: mały.
Trasa startuje z Rynku, po bruku, jedziemy malowniczym wąwozem pomiędzy Wzgórzem Zamkowym, a Górą Trzech Krzyży, uwaga na duży ruch turystyczny.
comments=Dnia 2014-04-19 użytkownik chirality dodał:
Nawierzchnia z kamieni wybija z rytmu. W połowie podjazdu kilkumetrowe wypłaszczenie - ulga. Da się uniknąć największych gradientów, biorąc zakręt w prawo szerokim łukiem. Uwaga na ruch samochodowy.
Dnia 2014-06-28 użytkownik Narzew dodał:
Nawierzchnia jest beznadziejna. Nie jest to szosa, tylko to są kocie łby. Droga jest historyczna, moim zdaniem kilkuset letnia.
author=Maćko
segment=0
locations=[[1,51.32257453936516,21.948860129222],[2,51.32286978694684,21.950097738745853],[3,51.32347820249796,21.95099092144619],[4,51.3241610699451,21.951576419472985],[5,51.3243878366428,21.952833231019554],[6,51.32434014822367,21.95412848910655],[7,51.32381063511361,21.955057491934895],[8,51.32353363776581,21.95611988877704],]
podstawa=[[15,51.3243878366428,21.952833231019554],['start',51.3221800,21.9478300],['meta',51.3235600,21.9574500]]
daneflot=[[0.0,130.479],[0.1,139.016],[0.2,154.518],[0.3,161.816],[0.4,165.941],[0.5,183.607],[0.6,197.580],[0.7,202.572],[0.8,206.808],[0.9,209.554],]