name=Małobądz od Sławkowa
slope=4.4km 1.8%
points=29
region=59
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: niewielki.
Ciekawy podjazd, zaczyna się przy jeziorku za Sławkowem, biegnie przy rzece, warto go mieć w kolekcji, no i 359m n.p.m. wysokości to nieźle jak na okolicę. W połączeniu z podjazami Podlipie i Bukowno można tworzyć tu ekstra pętle 12-15 km, z ok 150m przewyższenia.
comments=Dnia 2015-01-08 użytkownik szymek dodał:
Proponowałbym zaraz za wiaduktem skręcić w prawo w ulicę Fransceso Nullo. Po drodze z lewej strony jest jeden budynek przy którym jest stary zabytkowy cmentarz żydowski. Polecam zatrzymać się na chwilę i zobaczyć. Potem wąska asfaltowa uliczka wspina się aż do kościoła w Krzykawce. Dalej droga łączy się z twoim podjazdem. Ta trasa jest o kilometr krótsza przy tym samym początku i końcu trasy. Jeżeli nie jechałeś to polecam jeszcze ulicę Miodową od Kuźniczki. Jest odcinek z płyt betonowych (szosówką jechałem) i polecam. Pozdrawiam.
author=xtnt
segment=0
locations=[[1,50.29974114300047,19.402191712378226],[2,50.30147712778493,19.40193388216892],[3,50.30307804561114,19.40078396169997],[4,50.3046682123021,19.399644857547855],[5,50.30639758846487,19.39911589689609],[6,50.30811304422343,19.39870454550112],[7,50.30987692631742,19.398733088868994],[8,50.31162944193301,19.39901426695951],[9,50.31332202200063,19.399700026981463],[10,50.31386943429959,19.402084909612086],[11,50.314260540287,19.404754872286958],[12,50.31368156333105,19.407357150898633],[13,50.31297649978772,19.40988754044679],[14,50.31261213013866,19.412578267082154],[15,50.31231439893105,19.415294708638953],[16,50.31177447432167,19.41791915455451],[17,50.31115575437607,19.42048439380767],[18,50.31031292920031,19.422910103475374],[19,50.30951530386621,19.425373577711298],[20,50.30866799496871,19.427796152049837],[21,50.30781945245626,19.430217547616166],]
podstawa=[[14,50.309,19.42],['start',50.2997800,19.3995700],['meta',50.3067500,19.4323700]]
daneflot=[[0.0,281.244],[0.2,285.437],[0.4,285.995],[0.6,286.440],[0.8,287.904],[1.0,288.041],[1.2,288.144],[1.4,293.937],[1.6,296.536],[1.8,293.296],[2.0,303.170],[2.2,309.634],[2.4,322.984],[2.6,332.105],[2.8,332.793],[3.0,337.238],[3.2,342.746],[3.4,347.053],[3.6,344.841],[3.8,348.167],[4.0,352.384],[4.2,355.733],[4.4,358.560],]