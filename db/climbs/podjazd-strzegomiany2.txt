name=Strzegomiany od Letniskowej
slope=0.9km 7.7%
points=72.9
region=9
description=Nawierzchnia: szosa, stan bardzo dobry. Ruch samochodowy: minimalny.
To najbardziej stromy kawałek asfaltu w okolicach Wrocławia. Niestety szkoda, że taki krótki i z malutkim zjazdem. Trasa idzie wzdłuż domków jednorodzinnych, praktycznie bez ruchu samochodowego. Szczerze polecam tę trasę.
comments=
author=???
segment=0
locations=[[1,50.87939054588053,16.748499359106063],[2,50.87880642060676,16.747509136733015],[3,50.87863775359638,16.7461437996767],[4,50.8786218237564,16.744746665619573],[5,50.87859956232941,16.743349728684052],[6,50.87908775149743,16.742699342856895],[7,50.87903108701863,16.74174123569776],[8,50.87872906348843,16.74042886368295],]
podstawa=[[16,50.87975205350594,16.744462251663208],['start',50.8801200,16.7492700],['meta',50.8785000,16.7391000]]
daneflot=[[0.0,229.719],[0.1,234.088],[0.2,238.073],[0.3,245.689],[0.4,255.496],[0.5,264.751],[0.6,260.455],[0.7,270.685],[0.8,283.285],[0.9,297.123],]