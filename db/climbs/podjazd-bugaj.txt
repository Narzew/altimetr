name=Bugaj
slope=0.9km 7.9%
points=63.6
region=22
description=Nawierzchnia: szosa, stan bardzo dobry. Ruch samochodowy: minimalny.
Krótki podjazd w całości przebiegający w lesie. Na początku łagodny, a w końcówce daje się już mocniej we znaki. Kończy się na skraju lasu i od razu zaczyna się szybki zjazd - niemal na końcu drogę przecinają tory kolejowe (warto uważać, żeby przez nie nie przelecieć).
comments=
author=Artur85
segment=0
locations=[[1,49.84592298805817,19.664007183747344],[2,49.845032010713,19.66368160307468],[3,49.84415857981009,19.663270004618653],[4,49.84329486539225,19.66279881848652],[5,49.84245072681055,19.662246292838972],[6,49.84154755641023,19.662191791626128],[7,49.84076974115175,19.661549243047148],[8,49.84005058826789,19.660669018000817],]
podstawa=[[14,49.84245072681055,19.662246292838972],['start',49.8468100,19.6637900],['meta',49.8392600,19.6599900]]
daneflot=[[0.0,381.871],[0.1,383.494],[0.2,389.205],[0.3,399.297],[0.4,411.453],[0.5,419.395],[0.6,428.889],[0.7,436.961],[0.8,446.365],[0.9,452.986],]