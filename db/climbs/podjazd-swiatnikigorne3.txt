name=Świątniki Górne od Wrząsowic
slope=2.8km 3.6%
points=48.1
region=41
description=Nawierzchnia: szosa, stan zmienny. Ruch samochodowy: średni.
Ciężko to nazwać podjazdem, jednak między startem, a metą występuje widoczne przewyższenie. Z początku asfalt kiepskiej jakości, im dalej, tym lepiej.
Końcówka po nowiutkiej, gładkiej warstwie. Pagórek zdecydowanie na dużą tarczę.
comments=Dnia 2015-06-01 użytkownik Koler dodał:
Może zamiast dwóch podjazdów w bazie ( Świątniki Górne od Wrząsowic oraz Wrząsowice od Swoszowic) połączyć je w jeden: Świątniki Górne od Swoszowic?
author=qwerty
segment=0
locations=[[1,49.9530656893604,19.9473754282526],[2,49.95118769106868,19.94787625260267],[3,49.94935959032186,19.947073322622373],[4,49.94748195883581,19.94681146912012],[5,49.94569873237516,19.945538278421054],[6,49.94383692911364,19.944603925541287],[7,49.9421011867842,19.943191184017678],[8,49.94027337333268,19.942114919670985],[9,49.93866813825495,19.94287471397979],[10,49.93731991309049,19.94506002900812],[11,49.93541975561625,19.944359899599704],[12,49.93442793209243,19.945807047215567],[13,49.9339502543229,19.94876285379928],]
podstawa=[[13,49.95,19.942114919670985],['start',49.9545200,19.9453200],['meta',49.9338300,19.9517300]]
daneflot=[[0.0,281.547],[0.2,291.176],[0.4,298.929],[0.6,311.916],[0.8,314.393],[1.0,320.306],[1.2,321.512],[1.4,322.458],[1.6,328.806],[1.8,338.513],[2.0,349.222],[2.2,360.943],[2.4,361.937],[2.6,370.118],[2.8,381.765],]