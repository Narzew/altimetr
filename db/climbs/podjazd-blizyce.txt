name=Bliżyce
slope=3.0km 3.1%
points=38.2
region=61
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Podjazd w kierunku Zdowa
comments=Dnia 2014-10-24 użytkownik xtnt dodał:
Trochę słaby podjazd (seria podjazdów), byłem ciekaw kiedy ktoś doda, warto go pokonać ze względu na bezpieczny i bardzo szybki zdjazd na Zdów (prędkości powyżej 80 na MTB) i niesamowity widok. 2gie miejsce pod względem przewyższenia (92m), zaskakująco słaba jest Wyżyna Częstochowska pod tym względem, ale sam Zdów w drugą stronę jest dość stromy, więc ogólnie nie brakuje stromizn tylko długości.
Dnia 2014-10-24 użytkownik xtnt dodał:
PS. Zdów w drugą stronę ma 382m n.p.m., de facto szczyt obu podjazdów jest w tym samym miejscu, więc zginęło gdzieś 6 metrów.
Dnia 2014-10-24 Admin dodał:
Nic nie zginęło podjazd Zdów jest około 1200m dalej.
author=Parker
segment=0
locations=[[1,50.64562849722937,19.562973518532203],[2,50.6439228743665,19.562251421561314],[3,50.64221693771264,19.561533468473613],[4,50.6405099062754,19.560824505601317],[5,50.63879939476918,19.560132594013453],[6,50.63709634007932,19.55939605273909],[7,50.63538719354332,19.55869520344254],[8,50.63367828028492,19.557992894555014],[9,50.63196877289704,19.557294192312156],[10,50.63025608745367,19.556615387514967],[11,50.62854293874195,19.555939829303725],[12,50.62684303177851,19.555185409803016],[13,50.6253913510922,19.553701312580188],[14,50.62408048437553,19.55183595225708],]
podstawa=[[13,50.63367828028492,19.557992894555014],['start',50.6473400,19.5636600],['meta',50.6227700,19.5499700]]
daneflot=[[0.0,284.000],[0.2,291.433],[0.4,301.063],[0.6,304.904],[0.8,304.248],[1.0,308.694],[1.2,316.397],[1.4,331.980],[1.6,340.581],[1.8,345.421],[2.0,352.172],[2.2,356.635],[2.4,363.026],[2.6,370.149],[2.8,374.678],[3.0,376.285],]