name=Borek Stary - Klasztor
slope=1.2km 7.4%
points=77.9
region=47
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: mały.
Ciekawy podjazd pod klasztor o.o. dominikanów i znajdujące się obok sanktuarium Matki Boskiej Boreckiej. Po drodze mijamy sam zespół klasztorny i malownicze kapliczki leżące tuż przy drodze, za ostatnią z nich kończy się asfalt. Z góry fantastyczny widok na dolinę Strugu, Kielnarową i Borek. Przy zjeździe trzeba bardzo uważać na piaskowe "łaty" rozsiane po całej nawierzchni jezdni.
comments=
author=Barnaba
segment=0
locations=[[1,49.96026203543831,22.092552245414936],[2,49.95952280671091,22.091822806112987],[3,49.9588042085532,22.091046576492772],[4,49.95826323137407,22.089978222803666],[5,49.95773950468237,22.08888743489331],[6,49.9574488953633,22.08764659919848],[7,49.95716778476252,22.086386969678415],[8,49.95669242326056,22.08524427917382],[9,49.95632978234061,22.084011845971077],[10,49.95554748947202,22.08392898719444],[11,49.95489558558728,22.08302082460466],]
podstawa=[[14,49.95716778476252,22.086386969678415],['start',49.9610100,22.0932600],['meta',49.9540900,22.0826600]]
daneflot=[[0.0,219.644],[0.1,220.095],[0.2,222.120],[0.3,229.617],[0.4,237.156],[0.5,244.411],[0.6,252.600],[0.7,260.040],[0.8,269.527],[0.9,278.289],[1.0,290.348],[1.1,300.587],[1.2,308.850],]