name=Wola Kocikowa
slope=1.3km 3.9%
points=28.1
region=61
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Niezbyt wymagający podjazd o przyzwoitej nawierzchni i minimalnym ruchu samochodowym. Przejeżdżamy na długość całą wieś, poczatkowo bardzo łagodnie a po minięciu ogrodzenia szkoły (drabinki, chuśtawki, siatka) odbijamy w prawo i nachylenie rośnie (do okolic 10%). Podjazd pozwala jadąc z Pilicy do Ogrodzieńca skrócić choć trochę odcinek po ruchliwej drodze wojewódzkiej nr 790.
comments=
author=Stradovius
segment=0
locations=[[1,50.45886876581537,19.616369590637078],[2,50.4594264170381,19.61535782180772],[3,50.45992316249288,19.61426285414734],[4,50.46029216642361,19.613048648147696],[5,50.46062698519404,19.611808855565755],[6,50.46096986536532,19.610574498603683],[7,50.46138917418012,19.60940530382379],[8,50.46189245787791,19.608315695993383],[9,50.46268534277527,19.60801458316132],[10,50.46351326771882,19.60836174334986],[11,50.46435044336346,19.608652191751617],[12,50.46511589223942,19.608365464562212],]
podstawa=[[14,50.46138917418012,19.60940530382379],['start',50.4580400,19.6166600],['meta',50.4658000,19.6075600]]
daneflot=[[0.0,375.862],[0.1,378.721],[0.2,380.805],[0.3,383.519],[0.4,385.000],[0.5,387.094],[0.6,388.251],[0.7,389.936],[0.8,392.235],[0.9,395.743],[1.0,402.518],[1.1,411.460],[1.2,419.949],[1.3,425.852],]