name=Kondraty od Gródek
slope=1.0km 5.4%
points=36.5
region=83
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: mały.
Wąska droga, na asfalcie u podnóża zalegały spore pokłady piasku, dlatego warto uważać przy zjeździe. Trasa w dość ładnej scenerii, ruch samochodowy mały. Asfalt lekko spękany, ale większych dziur tutaj nie było. Trasa w ładnej scenerii.
comments=
author=Narzew
segment=7359314
locations=[[1,50.75596053249745,22.727419028394934],[2,50.75565227441512,22.726462394875284],[3,50.75526866653918,22.725251740119575],[4,50.75500699018892,22.72397514302395],[5,50.7550595138288,22.72256869188209],[6,50.7551057492181,22.721161710142496],[7,50.7546261117167,22.72001767886843],[8,50.75392394243721,22.719236788470425],[9,50.75303963219802,22.719397188022867],]
podstawa=[[15,50.7551057492181,22.721161710142496],['start',50.7568400,22.7271900],['meta',50.7521500,22.7194100]]
daneflot=[[0.0,254.137],[0.1,258.913],[0.2,264.447],[0.3,270.767],[0.4,274.343],[0.5,277.070],[0.6,281.329],[0.7,291.442],[0.8,301.553],[0.9,306.789],[1.0,308.148],]