name=Góra Siewierska od Twardowic
slope=0.8km 4.6%
points=23
region=59
description=Nawierzchnia: szosa, stan słaba. Ruch samochodowy: minimalny.
Jedna z kilku wersji podjazdu na Górę Siewierską. Pod względem trudności porównywalna z wariantem podjazdu przedstawionym na "Altimetrze" od strony Goląszy Dolnej, przez Brzękowice Wał. Z jedną różnicą. Na zjeździe z centrum Góry Siewierskiej (od remizy) w kierunku Twardowic można przekroczyć 70 km/h przy stosunkowo dużym prawdopodobieństwie przeżycia (mały ruch na jezdni, brak zabudowy w dolnym odcinku). To jedno z niewielu takich miejsc w tej części Wyżyny Śląskiej pozwalające na takie osiągi, ale w tym momencie zmieniłem temat na zjazdy, a miało być pod górę. Podjazd polecam szczególnie w okresie przygotowywania formy wiosną lub jesiennego zmęczenia sezonem. Można się nieźle napocić. Niestety od pewnego czasu asfalt jest tu słabej jakości.
comments=
author=Kokosz
segment=0
locations=[[1,50.4082313618172,19.08544403863982],[2,50.40732285798519,19.085409551601742],[3,50.40641539159076,19.085340695684636],[4,50.40553120281915,19.085020343347423],[5,50.40465495443762,19.084642361262922],[6,50.40382258267611,19.084102090007946],[7,50.40306706301239,19.08331242997542],]
podstawa=[[15,50.407,19.084642361262922],['start',50.4091400,19.0854700],['meta',50.4022300,19.0827600]]
daneflot=[[0.0,333.631],[0.1,333.825],[0.2,337.186],[0.3,341.685],[0.4,348.348],[0.5,355.476],[0.6,363.327],[0.7,369.570],[0.8,370.974],]