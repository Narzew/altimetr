name=Czarna Góra (Tatry) od Jurgowa
slope=1.2km 8.7%
points=105.5
region=23
description=Nawierzchnia: szosa, stan średni/dobry. Ruch samochodowy: mały.
Krótka i stroma ścianka. Z każdym pokonanym odcinkiem widoki na Tatry robią się coraz ładniejsze. Niestety, w pierwszej części podjazdu droga jest dość dziurawa, a oprócz tego można tam spotkać sporo nieuwiązanych piesków.
comments=Dnia 2014-09-18 użytkownik Krissj dodał:
Wkradł się tu mały błąd - alternatywą dla tego podjazdu powinna być "Czarna Góra - od Trybsza", a nie "Czarna Góra - od Pławnicy", która to jest w Sudetach. Pozdrawiam
Dnia 2014-09-22 Admin dodał:
Tak masz rację. Już poprawiam.
author=Krissj
segment=0
locations=[[1,49.36962914773117,20.123554360713115],[2,49.37045331804232,20.12406479457991],[3,49.37118197892278,20.124807960651424],[4,49.37114612229401,20.12612809644952],[5,49.37087150444876,20.127427731963053],[6,49.37063402225984,20.128743854557342],[7,49.37081571953663,20.12965064752825],[8,49.37162174524428,20.129084245730724],[9,49.37245343960357,20.12876477470502],[10,49.37329108322762,20.129214948525487],[11,49.37414905477912,20.12956743274492],]
podstawa=[[14,49.37081571953663,20.12965064752825],['start',49.3689600,20.1227500],['meta',49.3749900,20.1300100]]
daneflot=[[0.0,727.089],[0.1,732.211],[0.2,737.836],[0.3,747.985],[0.4,757.863],[0.5,763.573],[0.6,770.223],[0.7,779.342],[0.8,783.978],[0.9,796.976],[1.0,813.600],[1.1,825.626],[1.2,831.075],]