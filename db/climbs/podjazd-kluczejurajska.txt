name=Klucze (ul. Jurajska)
slope=1.0km 4.0%
points=22.1
region=62
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Taki sobie podjazd od ronda jadąc na Jaroszowiec skręcamy w prawo w ul. Jurajską. Warto tam jechać poza podjazdem z tych powodów, że widać fajną panoramę wstecz na podjeździe, oraz na szczycie są skałki, dwa rewelacyjne punkty widokowe. Szosą trzeba wracać tą samą drogą, MTB może sobie pozwolić na zjazd jadąc dalej ul. Polną.
comments=
author=xtnt
segment=0
locations=[[1,50.33558213482535,19.56389898022553],[2,50.33567053647835,19.56528327650335],[3,50.33544937376205,19.566574905588823],[4,50.33491104115726,19.56767481249915],[5,50.33424498039849,19.56859807744911],[6,50.33352449276971,19.5694107848999],[7,50.33276059958802,19.57012358960924],[8,50.3324991783229,19.57126360880102],[9,50.33250700994542,19.572656727953472],]
podstawa=[[15,50.33352449276971,19.5694107848999],['start',50.3357100,19.5626300],['meta',50.3325000,19.5740500]]
daneflot=[[0.0,352.654],[0.1,355.498],[0.2,358.012],[0.3,361.192],[0.4,366.875],[0.5,375.238],[0.6,383.507],[0.7,384.558],[0.8,387.503],[0.9,390.606],[1.0,393.036],]