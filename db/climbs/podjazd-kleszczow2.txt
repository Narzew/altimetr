name=Kleszczów od Aleksandrowic
slope=1.7km 5.3%
points=74.9
region=64
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Mały Gliczarów, koło Krakowa. Na początku (szacunkowo - mierzone bólem nóg) 8-10% potem 150 m jeszcze stromiej i przechodzi dalej w 8-12%... Krótki i treściwy, 10-15km od Krakowa
comments=Dnia 2014-02-06 użytkownik Amator88 dodał:
profil nie do końca oddaje ten podjazd, w 400-500 metrów od początku jest 100 m ścianka płaczu o nachyleniu zbliżonym do najstromszych części podjazdu pod Gliczarów, gdzie znaki mówią o 20%. Może przez to, że jest to w lesie to mapka się trochę gubi. Warto też zjechać sobie z Kleszczowa do Balic. Zjazd w pierwszej części bardzo szybki i bezpieczny - można sobie wykręcić v-maxa :) potem bardziej kręty i techniczny. Z Balic można skręcić z powrotem na Aleksandrowo i robić sobie pętle, która ma 7-9 km. Bardzo fajny trening.
author=Amator88
segment=0
locations=[[1,50.09368302327699,19.751098716747492],[2,50.09455178599333,19.75092500848814],[3,50.09541970641497,19.750845424926865],[4,50.0962424885773,19.75128401923041],[5,50.0970993880561,19.751033863096723],[6,50.09786635737751,19.75049120988251],[7,50.09843996460476,19.749479552616094],[8,50.09922489839627,19.74887310536144],[9,50.09999882389398,19.74825541171208],[10,50.10086155871453,19.7484094336628],[11,50.10172559413452,19.748624946512905],[12,50.10258817999181,19.748780362081675],[13,50.10341039753172,19.74876370633376],[14,50.10426458020554,19.748778267486955],[15,50.10509421390756,19.74915232898138],[16,50.10558955175702,19.75027898044459],]
podstawa=[[14,50.09999882389398,19.74825541171208],['start',50.0928100,19.7512100],['meta',50.1063000,19.7503900]]
daneflot=[[0.0,265.025],[0.1,270.321],[0.2,281.114],[0.3,290.891],[0.4,303.454],[0.5,309.950],[0.6,313.691],[0.7,320.131],[0.8,329.895],[0.9,337.417],[1.0,342.963],[1.1,342.296],[1.2,337.693],[1.3,338.964],[1.4,344.455],[1.5,348.276],[1.6,351.839],[1.7,355.018],]