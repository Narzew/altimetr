name=Wzgórze Widok las Bukowy
slope=1.0km 5.8%
points=42.2
region=52
description=Nawierzchnia: teren, dobrej jakości ścieżka żwirowa
Świetna trasa, szkoda tylko, że taka krótka. Podjazd warto zacząć już od parkingu prosto główną ścieżką zdrowia. Po około 300m skręcić w lewo w leśną ścieżkę przy bramie wejściowej przy stawach. Później bardzo ostro pod górę, aż do skrzyżowania leśnych ścieżek. Na skrzyżowaniu skręcić w prawo. Do końca podjazdu jest jeszcze około 400m, ale już nieco lżej. Ścieżki w lesie są bardzo dobrej jakości. Szczyt jest w  środku lasu bukowego.
comments=
author=???
segment=0
locations=[[1,51.3027263382419,17.069497017927233],[2,51.30191613256682,17.070089712037316],[3,51.30109881816615,17.070662679881707],[4,51.30054369980136,17.07173645244052],[5,51.30042452692887,17.07313817762406],[6,51.30036355539201,17.07456264308962],[7,51.30000727503702,17.075816036305696],[8,51.29914974865635,17.076215681380404],[9,51.29828303663538,17.076561412243336],]
podstawa=[[15,51.30036355539201,17.07456264308962],['start',51.3034500,17.0687300],['meta',51.2974200,17.0769300]]
daneflot=[[0.0,191.601],[0.1,192.484],[0.2,198.249],[0.3,205.903],[0.4,213.356],[0.5,224.866],[0.6,232.384],[0.7,236.725],[0.8,243.513],[0.9,247.011],[1.0,249.531],]