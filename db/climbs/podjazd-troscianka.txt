name=Trościanka
slope=0.9km 5.0%
points=27.4
region=82
description=Nawierzchnia: szosa, stan średni. Ruch samochodowy: minimalny.
Męczący podjazd na Działach Grabowieckich. Ciekawostka geologiczna: zaraz na początku podjazdu po lewej stronie kredowa wychodnia, w której można znaleźć skamieniałe rostra belemnitów.
comments=
author=roch
segment=0
locations=[[1,50.90174560683779,23.49394738163096],[2,50.9011632781418,23.495044781044726],[3,50.90060621476263,23.49617482608369],[4,50.90033986750193,23.497485820124325],[5,50.89967511602453,23.4983762888005],[6,50.89914568855941,23.49951943751239],[7,50.89863265634062,23.50068841432335],[8,50.89778824372408,23.50112141226691],]
podstawa=[[15,50.89967511602453,23.4983762888005],['start',50.9024300,23.4930400],['meta',50.8968900,23.5012900]]
daneflot=[[0.0,252.045],[0.1,253.566],[0.2,259.286],[0.3,263.744],[0.4,267.110],[0.5,271.990],[0.6,281.702],[0.7,288.931],[0.8,294.599],[0.9,296.930],]