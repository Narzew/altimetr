name=Zamarski  od Kostkowic
slope=1.2km 7.3%
points=87.9
region=40
description=Nawierzchnia: szosa, stan słąby. Ruch samochodowy: minimalny.
Jak na region podgórski, jeden z trudniejszych podjazdów szosowych. Byłem nawet zaskoczony jak ładnie "trzyma". Szkoda tylko, że nie najlepszy asfalt, ale przy prędkości rzędu 10, a czasem nawet 7-8 km/h nie ma to aż takiego znaczenia...Górka wśród drzew, więc widoki słabe, ale malowniczy zjazd do Cieszyna (o ile mamy rower MTB, bo na zjeździe kostka). Dla relaksu i odpoczynku przyda się chwilka kontemplacji przy drewnianym kościółku w Zamarskach.
comments=
author=Kokosz
segment=0
locations=[[1,49.78296438172836,18.68350257061479],[2,49.78283477080588,18.682120619752595],[3,49.78265571751884,18.680751580878905],[4,49.78248909354574,18.679379648669965],[5,49.78211088004152,18.67811307757836],[6,49.78178383138938,18.67684084328505],[7,49.7823324492415,18.675750000129938],[8,49.7826500277453,18.67444835995525],[9,49.78263605617868,18.67306351914158],[10,49.78227075182579,18.671831127243422],[11,49.78196278694639,18.67114745876438],]
podstawa=[[15,49.7823324492415,18.675750000129938],['start',49.7830600,18.6848900],['meta',49.7811300,18.6714800]]
daneflot=[[0.0,300.379],[0.1,303.585],[0.2,310.998],[0.3,318.619],[0.4,327.875],[0.5,343.206],[0.6,357.576],[0.7,359.161],[0.8,363.282],[0.9,373.851],[1.0,381.567],[1.1,383.639],[1.2,387.924],]