name=Pod Glisno z Lubniewic
slope=1.8km 4.1%
points=47.8
region=70
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: minimalny.
Jeden z Lepszych podjazdów  treningowych w Lubuskim do przygotowania formy podjazdowej. Ma  dwa dosyć strome odcinki. Pierwsze sto metrów łagodnie i wjazd do lasku stromo ok czterysta metrów nachylenie dochodzi tam do dziesięć procent i wypłaszczenie wyjazd mały ząb na złapanie kadencji i znowu stromiej ok sześć procent chwilę trzyma i lekko znowu  się wypłaszcza na sto metrów, dalej wjazd do lasku i stromo do jedenastu procent trzyma, i wjazd na garb na końcu lekkie nachylenie nie odczuwalne po tym co było wcześniej.
comments=
author=Guzi Team
segment=7098953
locations=[[1,52.5030525769501,15.241742576661181],[2,52.50219600295693,15.241560582664533],[3,52.50131571853334,15.241627248555915],[4,52.50044431313169,15.241817843908166],[5,52.4995922747614,15.242177210294813],[6,52.49871113295388,15.242182418290895],[7,52.49787320172732,15.242624731362639],[8,52.49713352904372,15.243391132451961],[9,52.49644745781971,15.24429984695098],[10,52.4957525087612,15.24519054970824],[11,52.49505797716305,15.246082116853131],[12,52.49435355482385,15.24695165189678],[13,52.49353245240283,15.247455930355272],[14,52.49267632719033,15.247800453804189],[15,52.49181881868508,15.24813581631679],[16,52.49096004180092,15.248461986043594],[17,52.49008837884312,15.24858107582827],]
podstawa=[[14,52.4957525087612,15.24519054970824],['start',52.5037800,15.2425600],['meta',52.4892100,15.2484600]]
daneflot=[[0.0,49.484],[0.1,51.491],[0.2,55.530],[0.3,63.235],[0.4,71.633],[0.5,78.502],[0.6,82.173],[0.7,82.181],[0.8,81.492],[0.9,81.180],[1.0,83.355],[1.1,89.077],[1.2,92.985],[1.3,95.470],[1.4,96.780],[1.5,98.605],[1.6,106.307],[1.7,115.887],[1.8,123.064],]