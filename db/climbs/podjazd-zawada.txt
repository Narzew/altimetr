name=Zawada
slope=0.9km 7.6%
points=60
region=46
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: mały.
Początek łatwy. Dalej trochę trudniej. Następnie wypłaszczenie. Na rozwidleniu dróg skręcając w lewo dojechać można do drogi Zawada-Stobierna. Zjeżdżając trzeba uważać na zakrętach i na betonowe rynny w dwóch miejscach w asfalcie.
comments=
author=thedk63
segment=0
locations=[[1,50.05405171706136,21.47712160863398],[2,50.05322699971892,21.477634943143357],[3,50.05235867814412,21.47800865792192],[4,50.05148075741532,21.478221685033986],[5,50.05095984979151,21.47725725017858],[6,50.05023596798511,21.47647834439374],[7,50.04934040725433,21.476359692755977],[8,50.04852616355651,21.476727689226323],]
podstawa=[[15,50.052,21.47725725017858],['start',50.0548200,21.4764100],['meta',50.0480300,21.4779000]]
daneflot=[[0.0,208.947],[0.1,211.171],[0.2,215.871],[0.3,224.774],[0.4,233.911],[0.5,239.059],[0.6,247.936],[0.7,259.294],[0.8,270.803],[0.9,277.433],]