name=Dziewicza Góra z Sączowa
slope=0.6km 6.8%
points=35.5
region=59
description=Nawierzchnia: szosa, stan bardzo dobry. Ruch samochodowy: minimalny.
Najtrudniejszy podjazd na Garbie Tarnogórskim wchodzącym w skład Wyżyny Śląskiej. Z góry, przy odpowiedniej pogodzie, znakomite widoki na rejon lotniska w Pyrzowicach aż po Próg Woźnicki. W wyjątkowych przypadkach widać Górę Kamieńsk k. Bełchatowa (!)
comments=
author=Kokosz
segment=0
locations=[[1,50.43206033477927,19.05043796574637],[2,50.43294066768166,19.05090594889566],[3,50.43382099870704,19.051373949449044],[4,50.43456906209921,19.052193358499153],[5,50.43529976977479,19.05306805876205],]
podstawa=[[15,50.43456906209921,19.052193358499153],['start',50.4311800,19.0499700],['meta',50.4361800,19.0535300]]
daneflot=[[0.0,310.386],[0.1,313.447],[0.2,318.820],[0.3,327.506],[0.4,339.210],[0.5,348.999],[0.6,351.706],]