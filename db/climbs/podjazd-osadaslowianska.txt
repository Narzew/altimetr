name=Osada Słowiańska ze Stobiernej
slope=1.2km 9.1%
points=121.5
region=46
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: znikomy.
Niesamowicie wymagający podjazd, choć krótki. Prowadzi do Osady Słowiańskiej w Stobiernej, a zaczyna się niedaleko szkoły, przy przystanku autobusowym. Jadąc od Zawady skręcamy w lewo. Początek pokazuje pazur, ale to jeszcze nie to. Bardzo wąska droga, ruch znikomy, ale uwaga na zakrętach (jeden jest ślepy). Po minięciu kilku domów zaczyna się prawdziwy hardcore Po lewej widać osadę, chwila odpoczynku gdyż nachylenie na chwilę maleje, po to tylko żeby znów wzrosnąć i trzymać aż do pierwszych zabudowań na szczycie. 
Zjazd ze względu na zakręty i wąską droge niesamowicie niebezpieczny.
comments=
author=TomekJac
segment=0
locations=[[1,50.02985551818626,21.49110344564383],[2,50.02923583785253,21.492166279694743],[3,50.02838984779592,21.492808460406422],[4,50.0275818166655,21.493564547672122],[5,50.02774941087888,21.49492471342751],[6,50.02746180884889,21.49632215210488],[7,50.02752514546169,21.497549329912772],[8,50.02760674300921,21.498842123005033],[9,50.0273975391315,21.50026984390945],[10,50.02696280901988,21.501570418627807],[11,50.02650435985084,21.502855008807842],]
podstawa=[[14,50.02752514546169,21.497549329912772],['start',50.0299000,21.4897100],['meta',50.0260000,21.5040800]]
daneflot=[[0.0,256.368],[0.1,261.316],[0.2,271.038],[0.3,278.192],[0.4,283.148],[0.5,299.725],[0.6,313.171],[0.7,325.266],[0.8,337.765],[0.9,351.124],[1.0,358.723],[1.1,362.097],[1.2,365.770],]