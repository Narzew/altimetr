name=Lanckorona od południa
slope=2.0km 5.5%
points=98.8
region=22
description=Nawierzchnia: szosa, stan bardzo dobry. Ruch samochodowy: minimalny.
Początek jest dość łagodny, ale nachylenie szybko wzrasta. Potem do końca pierwszego kilometra szosa prowadzi ostro pod górę. Dalej jest niewielkie siodło i końcówka ponownie wita nas dużą stromizną. Całość prowadzi po asfalcie bardzo dobrej jakości.
comments=
author=Greek
segment=2392692  
locations=[[1,49.83106324798025,19.716918152201174],[2,49.83249105176026,19.718215214590145],[3,49.83390314578454,19.71963426431182],[4,49.83529309908666,19.72122161239463],[5,49.83702903098433,19.721496901581304],[6,49.83871001673445,19.72085999665319],[7,49.8401955254249,19.719476189386796],[8,49.84182870246197,19.718461335886786],[9,49.84344936542742,19.718010635810856],]
podstawa=[[14,49.83871001673445,19.72085999665319],['start',49.8299300,19.7149500],['meta',49.8450500,19.7169000]]
daneflot=[[0.0,348.902],[0.2,356.119],[0.4,374.670],[0.6,397.746],[0.8,415.657],[1.0,422.411],[1.2,419.303],[1.4,417.732],[1.6,428.436],[1.8,450.378],[2.0,459.189],]