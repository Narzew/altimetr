name=Cichawka
slope=0.8km 10.0%
points=88.3
region=42
description=Nawierzchnia: szosa. Ruch samochodowy: minimalny.
Krótki, ale zaskakująco sztywny podjazd na zupełnym końcu świata :) Niewątpliwym atutem jest znikomy ruch i piękna, spokojna okolica.
Bardzo dobry stan nawierzchni, dojazd do Cichawki od Wieruszyc w stanie średnim.
comments=
author=???
segment=0
locations=[[1,49.87165342539459,20.344503584542053],[2,49.87205867881504,20.345662335328598],[3,49.87274606886618,20.34637353920357],[4,49.87354685629363,20.3468148090667],[5,49.87432715998705,20.346412840276344],[6,49.87514327826779,20.346237324509843],[7,49.87593740627369,20.34669055438826],]
podstawa=[[15,49.87432715998705,20.346412840276344],['start',49.8710300,20.3437900],['meta',49.8764900,20.3464000]]
daneflot=[[0.0,240.472],[0.1,247.110],[0.2,255.815],[0.3,265.146],[0.4,271.543],[0.5,284.233],[0.6,299.581],[0.7,314.435],[0.8,318.796],]