name=Twarogi z Ochotnicy Dolnej
slope=1.9km 14.8%
points=456.1
region=27
description=Nawierzchnia: szosa, stan bardzo dobry. Ruch samochodowy: minimalny.
Trasa przez las
comments=Dnia 2014-10-10 użytkownik M dodał:
Niestety widać wyraźnie na zdjęciu lotniczym, że asfalt jest tylko na pierwszym odcinku podjazdu do około 0,9-1,0km.
Dnia 2014-10-11 użytkownik frazie dodał:
WitamZmierzyłem się dzisiaj  z tym podjazdem i go pokonałem na góralu, są chwile załamania ale zachęcam do zmierzenia się z tym HC.  zapewniam że asfalt jest do samej góry.Podaję stronkę z zarejestrowanym moim wyjazdem.<a href="https://www.endomondo.com/workouts/422805917/13530531">https://www.endomondo.com/workouts/422805917/13530531</a>Pozdrawiam wszystkich HC
Dnia 2014-10-14 Admin dodał:
 Ale trasa. 37km i 1200m w górę. Twarogi, <a href="http://www.altimetr.pl/podjazd-studzionki.html">Studzionki</a> i <a href="http://www.altimetr.pl/przelecz-knurowska.html">Przełęcz Knurowska</a> z dwóch stron. Trasa killer...
author=frazie
segment=0
locations=[[1,49.52859481835814,20.340577772475626],[2,49.52896757471228,20.339702175279058],[3,49.52981449983678,20.339489315281526],[4,49.53066155801719,20.33932790594156],[5,49.53150364860435,20.339572470848225],[6,49.53231095576953,20.340017806200308],[7,49.53315475203593,20.340263372302843],[8,49.53397525781498,20.340658663694512],[9,49.53431017628692,20.341341174271406],[10,49.53357818810828,20.341997314054424],[11,49.53331781427918,20.342924886553647],[12,49.53360349295688,20.344165941039137],[13,49.53429643910479,20.344830617861362],[14,49.53482841967115,20.345869904280562],[15,49.5351254181662,20.347095915130126],[16,49.53495783678191,20.348272415742485],[17,49.53436098214491,20.34922619560325],[18,49.53501385695652,20.3496604572631],]
podstawa=[[15,49.533,20.341997314054424],['start',49.5279900,20.3398000],['meta',49.5354700,20.3504600]]
daneflot=[[0.0,473.138],[0.1,487.689],[0.2,502.938],[0.3,521.574],[0.4,533.844],[0.5,553.217],[0.6,570.999],[0.7,586.787],[0.8,598.265],[0.9,611.055],[1.0,625.929],[1.1,651.716],[1.2,662.720],[1.3,680.182],[1.4,696.932],[1.5,708.934],[1.6,721.953],[1.7,727.877],[1.8,747.068],[1.9,755.225],]