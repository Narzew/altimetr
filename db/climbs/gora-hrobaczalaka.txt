name=Hrobacza Łąka
slope=3.8km 12.1%
points=596.9
region=21
description=Nawierzchnia: szosa, stan kiepski. Ruch samochodowy: minimalny.
Jeden z kultowych podjazdów Beskidu Małego, nie dający praktycznie żadnego wytchnienia. Asfaltowa droga początkowo niezłej jakości w drugiej części pozostawia niestety wiele do życzenia. Na szczycie znajduje się duży i charakterystyczny krzyż podświetlany nocą oraz schronisko. Przy dobrej widoczności imponująco prezentuje się z tego miejsca Babia Góra.
comments=Dnia 2014-11-16 użytkownik Koler dodał:
W tym wydaniu na pewno nie jest to podjazd szosowy. Asfalt kończy się po 2,0 km a dalej można jedynie wyprowadzić rower gdyż więcej tam żwiru i dziur niż asfaltu...
Dnia 2015-01-19 użytkownik jeznarowerze dodał:
Kolego Koler - sugeruję zapoznać się z historią TdF, Giro itp, lub obejrzeć w TV kultowy wyścig Paryż - Roubaix, aby zobaczyć po jakiej nawierzchni da się poruszać na rowerze szosowym.
Ale faktycznie Hrobaczą wygodniej zdobywać na góralu.
author=sagimike
segment=0
locations=[[1,49.80248626074231,19.191036754305514],[2,49.8028101463736,19.190030780639063],[3,49.80253509736432,19.187886459809533],[4,49.80370222932194,19.188051049334376],[5,49.80511803661543,19.186692475386053],[6,49.8065516779198,19.18522081326114],[7,49.80822194329883,19.18447613317221],[8,49.80929811929128,19.183363229819747],[9,49.81092937800243,19.183950311496346],[10,49.81235307245925,19.182443903357353],[11,49.81313198387789,19.18007336376661],[12,49.81333276842422,19.17737712541623],[13,49.81412605569799,19.175004079929522],[14,49.81570707418465,19.17389538452585],[15,49.81721850594256,19.17258041917603],[16,49.81864106731786,19.171129303495036],[17,49.81970909373897,19.169007803426553],[18,49.82101630113615,19.16724688729846],]
podstawa=[[13,49.81235307245925,19.182443903357353],['start',49.8038800,19.1926800],['meta',49.8219900,19.1650900]]
daneflot=[[0.0,361.439],[0.2,370.087],[0.4,398.375],[0.6,418.380],[0.8,441.845],[1.0,472.314],[1.2,496.919],[1.4,516.986],[1.6,539.769],[1.8,564.308],[2.0,585.008],[2.2,625.229],[2.4,657.012],[2.6,675.674],[2.8,694.837],[3.0,716.343],[3.2,742.382],[3.4,765.558],[3.6,789.160],[3.8,821.723],]