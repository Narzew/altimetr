name=Międzybrodzie Bialskie
slope=1.0km 11.4%
points=140.7
region=21
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: mały.
Fajna wspinaczka super asfaltem wśród zadbanych domów.
Z góry piękny widok na jezioro i Żar.
comments=Dnia 2014-08-17 użytkownik Koler dodał:
Podjazd na szosie odpada. Wyjechałem do pierwszego korytka ściekowego (odkrytego!) biegnącego w poprzek jezdni i zawróciłem. Zdecydowanie lepiej wybrać się na sąsiedni "Nowy świat" gdyż jest bardziej wymagający oraz bezpieczniejszy.
author=jeżnarowerze
segment=0
locations=[[1,49.78116831837182,19.19705300386488],[2,49.78071216876543,19.195976505591602],[3,49.78031384967142,19.194804832268233],[4,49.78019778865211,19.1935638578326],[5,49.78096089489943,19.193151712075746],[6,49.78170484621641,19.192556901288413],[7,49.78243548988304,19.19192210704034],[8,49.7824664469158,19.191347333499948],[9,49.78166263661565,19.190943064351586],]
podstawa=[[15,49.78170484621641,19.192556901288413],['start',49.7815300,19.1981200],['meta',49.7808900,19.1903800]]
daneflot=[[0.0,332.125],[0.1,340.818],[0.2,351.914],[0.3,367.109],[0.4,385.583],[0.5,396.607],[0.6,405.033],[0.7,411.193],[0.8,422.220],[0.9,435.115],[1.0,446.164],]