name=Vysoka Srbska  (Czechy)
slope=0.8km 10.6%
points=135.8
region=10
description=Nawierzchnia: bruk
Mityczny podjazd z wyścigu Specialized Sudety Tour. Podjazd, który zapada w pamięć każdemu kto się z nim zmierzy. Niezbyt długi podjazd, ale piekielnie stromy (w granicach 20%) ale pikanterii  dodaje fakt, iż droga jest wyłożona dużą nierówną kostką brukową.
"Podczas pierwszego (jak na razie jedynego) pokonywania tego podjazdu z wysiłku krzyczałem "To nie jest kolarstwo". Brakowało przełożenia, mimo, że miałem 34-28 i dosyć wysoką formę. Jazda w korbach, przepychanie rowera o centymetry, momentami rower stawał w miejscu i całą siłą trzeba było naciskać by przesunąć się dalej.
comments=
author=Bartek Zając
segment=0
locations=[[1,50.49413145604515,16.22779591508015],[2,50.49334243789644,16.228856361641306],[3,50.49251549844599,16.229814105629202],[4,50.49146308840065,16.229907454991007],[5,50.49041098111031,16.230011491074606],[6,50.48943622615695,16.22953084289429],[7,50.48870666114654,16.228334434225417],]
podstawa=[[15,50.49041098111031,16.230011491074606],['start',50.4945800,16.2263200],['meta',50.4878500,16.2276100]]
daneflot=[[0.0,409.353],[0.1,416.530],[0.2,416.276],[0.3,418.897],[0.4,428.256],[0.5,444.478],[0.6,469.091],[0.7,486.717],[0.8,495.732],]