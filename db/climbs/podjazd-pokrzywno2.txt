name=Pokrzywno drogą główną
slope=3.6km 4.9%
points=118.6
region=11
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: średni/mały.
Kolejny wariant dojazdu do Pokrzywna, tym zdecydowanie najdłuższy więc i średnie nachylenie będzie mniejsze. Początek podobnie jak poprzednio w Polanicy-Zdrój ale tym razem nie skręcamy w żadną w pobocznych ulic. Jedziemy ciągle główną szosą na Bystrzycę Kłodzką, przy okazji musimy pokonać 2 serpentyny zaraz na początku podjazdu. Dalej wspinamy się aż drogowskazu z napisem Pokrzywno, tam skręcamy w prawo. 
W końcowej fazie podjazdu przy dobrej pogodzie piękne widoki na całą Kotlinę Kłodzką od Gór Sowich aż po Masyw Śnieżnika.
comments=
author=Kornel (  http://kornel.bikestats.pl/)
segment=0
locations=[[1,50.3906955971862,16.523186405201386],[2,50.38901570604015,16.522758226285532],[3,50.38804895312775,16.522874324962572],[4,50.38667531900506,16.522670688515404],[5,50.38515154484932,16.52396712398945],[6,50.38347334492335,16.52480845619789],[7,50.38166943852942,16.524387745103695],[8,50.38063357153341,16.52660552386169],[9,50.37908843332972,16.528169198484647],[10,50.377574275538,16.52952281818534],[11,50.37722208266138,16.526700891801283],[12,50.37681009729761,16.52389485431604],[13,50.37685386369926,16.521052692748412],[14,50.37652926328864,16.518229675195244],[15,50.3770890560571,16.5156434621872],[16,50.37777216770191,16.51296889496348],[17,50.37839313283411,16.51025808373231],]
podstawa=[[14,50.386,16.522],['start',50.3922000,16.5229000],['meta',50.3786600,16.5074100]]
daneflot=[[0.0,384.814],[0.2,386.973],[0.4,398.750],[0.6,408.510],[0.8,421.427],[1.0,430.980],[1.2,444.239],[1.4,456.111],[1.6,466.726],[1.8,463.974],[2.0,462.348],[2.2,465.147],[2.4,474.770],[2.6,489.935],[2.8,504.029],[3.0,521.618],[3.2,537.131],[3.4,553.404],[3.6,563.136],]