name=Wzgórza Szpetalskie ul. Lipnowską na szczyt
slope=1.0km 7.2%
points=73.4
region=76
description=Nawierzchnia: szosa, stan zmienny. Ruch samochodowy: zmienny.
Podjazd rozpoczynamy na ul Lipnowskiej we Włocławku skręcamy w lewo na ul Obrońców Wisły 1920r po skręceniu nawierzchnia na początku 150m wyłożona kamieniem następnie 100m dobrego asfaltu, 50m twardej szutrówki i do końca prawie płyty betonowe lub chodnikowe, sama końcówka na ul. Chełmickiej znów asfalt. Wymagający dla kolarzówek ze względu na wyboistą nawierzchnie. 
 Ruch samochodowy jest na ulicy Lipnowskiej duży, a na ulicy Obrońców i Chełmickiej minimalny.
comments=
author=ArturGabrych
segment=0
locations=[[1,52.67012586515433,19.076819433306582],[2,52.66975171163941,19.078308841106264],[3,52.66937457868406,19.079796157812552],[4,52.66928531630573,19.081338790584823],[5,52.67011299517515,19.0819642723676],[6,52.67104375780539,19.082352372146033],[7,52.67173930043676,19.08348484536282],[8,52.67259026442168,19.083724391709325],[9,52.67351904824853,19.084009988609182],]
podstawa=[[15,52.67104375780539,19.082352372146033],['start',52.6705000,19.0753300],['meta',52.6740500,19.0853200]]
daneflot=[[0.0,60.253],[0.1,63.750],[0.2,66.716],[0.3,68.941],[0.4,70.496],[0.5,79.646],[0.6,91.047],[0.7,105.260],[0.8,118.206],[0.9,128.467],[1.0,132.544],]