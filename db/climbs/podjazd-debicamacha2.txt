name=Dębica (ul. Macha) od Stobiernej
slope=1.2km 6.2%
points=60.8
region=46
description=Nawierzchnia: szosa, stan zły. Ruch samochodowy: minimalny.
Na granicy Stobiernej i Stasiówki jest most na rzece (koło przystanku autobusowego). Trzeba go przejechać i skręcić w lewo jadąc równolegle wzdłuż rzeczki około 200-300 m. Dalej droga skręca w prawo i tu zaczyna się podjazd , niestety nie najlepszą drogą asfaltową (liczne nierówności i spękania).
comments=
author=thedk63
segment=0
locations=[[1,50.02395724696129,21.475416802086443],[2,50.02397127816599,21.47394737616878],[3,50.02412765597915,21.472490089805888],[4,50.02440540892561,21.471095389589095],[5,50.02483061406954,21.4697838286055],[6,50.02523718222373,21.46846039790057],[7,50.02565957705445,21.46714736540298],[8,50.02625730495953,21.465998345744538],[9,50.02689696675194,21.464909204465926],[10,50.0275972573486,21.463919866609103],[11,50.02828337836906,21.462934729927042],]
podstawa=[[14,50.02565957705445,21.46714736540298],['start',50.0233700,21.4763600],['meta',50.0288500,21.4617600]]
daneflot=[[0.0,282.789],[0.1,285.756],[0.2,293.837],[0.3,304.939],[0.4,312.885],[0.5,321.981],[0.6,330.858],[0.7,340.567],[0.8,347.476],[0.9,353.818],[1.0,355.765],[1.1,356.399],[1.2,357.254],]