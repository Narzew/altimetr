name=Łaziska Górne kościół
slope=1.3km 4.3%
points=29
region=59
description=Nawierzchnia: szosa, stan dobry. Ruch samochodowy: mały.
Podjazd od Huty Łaziska do Łazisk Górne Kościół. Nawierzchnia dobra, przejazd droga boczną przez osiedle domków jednorodzinnych.
comments=Dnia 2014-01-21 użytkownik Czarusblitz dodał:
Na tym podjezdzie trenuje czesto sile oraz wytrzymalosc silowa, wg mnie w okolicach Lazisk oraz Orzesza to najtrudnieszy podjazd mimo, ze jest krotszy od podjazdow na gore sw. Wawrzynca z ronda lub z Lazisk Sr. pod pomnik. Ten podjazd od samego poczatku trzyma i nie daje chwili tchu, a ostatni 300m odcinek po skrecie w lewo to jeszcze mocniejsza scianka. Warto sprobowac i wyrobic sobie wlasna opinie.
author=Czarusblitz
segment=0
locations=[[1,50.13861882679115,18.832249785528006],[2,50.13921937561691,18.831181661042592],[3,50.1396859555945,18.829964212051664],[4,50.14031120341148,18.829047341879914],[5,50.14105087495624,18.82874830643493],[6,50.14184964413876,18.829425646748177],[7,50.14271973168922,18.829876993174366],[8,50.14355874123102,18.83042863632943],[9,50.14399256391271,18.83168788576279],[10,50.14446270379248,18.83291044228929],[11,50.14525479944855,18.83344274241449],[12,50.14616608652693,18.833603337747718],]
podstawa=[[14,50.14271973168922,18.829876993174366],['start',50.1382200,18.8335300],['meta',50.1470500,18.8338100]]
daneflot=[[0.0,288.542],[0.1,289.968],[0.2,292.592],[0.3,294.841],[0.4,297.483],[0.5,301.733],[0.6,307.522],[0.7,312.871],[0.8,318.880],[0.9,324.562],[1.0,329.415],[1.1,336.413],[1.2,342.441],[1.3,345.870],]