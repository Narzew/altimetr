name=Jelna Działy  z Jelnej
slope=3.2km 6.6%
points=189.9
region=43
description=Nawierzchnia: szosa, stan kiepski. Ruch samochodowy: znikomy.
Ostry podjazd spod szkoły bezpośrednio na zbocze Kobylnicy Zachodniej. Maksymalne nachylenia osiągamy tuż po dotarciu pod ekspozycje zagajników, by po ich minięciu, wdrapać się na otwarte przełamanie przed masztem. Dalej podjazd nie nastręcza już większej komplikacji i w miarę swobodnie osiągamy wierzchowinę. Nawierzchnia nie najlepsza, wąsko. Niebezpiecznie na zjeździe przez zagajniki - naniesiony materiał leśny na jezdnię, a z prawej melioracyjna rynna. Uwaga też na pojawiające się znienacka samochody oraz turystów na przebiegającym tędy szlaku.
comments=
author=airstreeem20
segment=0
locations=[[1,49.70300741968154,20.72802548227412],[2,49.70442111497734,20.727934759937284],[3,49.7060254761634,20.72930454365917],[4,49.70758912886402,20.73061954368245],[5,49.70900048646132,20.732410000000073],[6,49.71082185021546,20.73276039675875],[7,49.71262718950506,20.733222093391305],[8,49.7143293309361,20.734256367075773],[9,49.71599765244412,20.73541549929132],[10,49.71715936164874,20.73755415509686],[11,49.71832395710691,20.739768573117544],[12,49.71906476905151,20.74156480586521],[13,49.71860939181412,20.744311615935658],[14,49.71808698283708,20.74703854871177],[15,49.71687159940259,20.749134876562266],]
podstawa=[[13,49.712,20.73541549929132],['start',49.7014400,20.7272900],['meta',49.7152000,20.7503300]]
daneflot=[[0.0,319.177],[0.2,338.012],[0.4,363.751],[0.6,391.515],[0.8,408.136],[1.0,427.785],[1.2,448.920],[1.4,464.531],[1.6,481.175],[1.8,485.367],[2.0,489.525],[2.2,492.727],[2.4,498.873],[2.6,501.748],[2.8,506.847],[3.0,518.671],[3.2,529.588],]