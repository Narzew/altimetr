name=Kościół  P. M. Pomocné  (okolice) (Czechy)
slope=1.2km 10.9%
points=187.8
region=14
description=Nawierzchnia: szosa, stan ogólnie dobry, ale miejscami zły. Ruch samochodowy: minimalny.
Trasa w większości prowadząca w kierunku urokliwego kościoła, lecz w pewnym momencie odchodząca w lewą stronę. W tym miejscu przykuwa uwagę krótki, ale za to wyróżniający się w tym miejscu przewyższeniem podjazd. Jego asfaltowa powierzchnia jest w gorszym stanie niż na pozostałych odcinkach, do tego jest węższa i otoczona wokoło drzewami. Wszystko to powoduje duży zastrzyk adrenaliny podczas zjazdu w dół na tym odcinku (prędkości mogą przekroczyć >80km/h).  Można zjechać również każdym innym rowerem, lecz należy przy zachowaniu odpowiedniej do sytuacji prędkości.
comments=
author=blindman
segment=0
locations=[[1,50.21707669835942,17.402972387523732],[2,50.21684648750412,17.401543940166448],[3,50.21684427276278,17.40006969093315],[4,50.21692000018865,17.398601573699125],[5,50.21745911475556,17.397660390089186],[6,50.21840305664264,17.39767965421015],[7,50.21934697029589,17.39767947880921],[8,50.21884421368196,17.396981140284424],[9,50.21806903876745,17.396145091093786],[10,50.21725910851821,17.39538879660506],[11,50.21638847943746,17.39485467409952],]
podstawa=[[15,50.21934697029589,17.39767947880921],['start',50.2166200,17.4040900],['meta',50.2155000,17.3952300]]
daneflot=[[0.0,669.482],[0.1,678.730],[0.2,691.478],[0.3,703.236],[0.4,710.412],[0.5,718.322],[0.6,719.259],[0.7,724.474],[0.8,736.708],[0.9,754.997],[1.0,773.294],[1.1,790.545],[1.2,800.605],]