description=Alpy Graickie to przede wszystkim Masyw Mont Blanc i najwyższy szczyt Alp mierzący 4810 m n.p.m. Alpy Graickie leżą przy granicy Francusko-Włoskiej. Turystycznie warto zwiedzić Chamonix, takie nasze Zakopane. W Chamonix można wjechać na najwyższą w europie kolej liniową na Aiguille du Midi nieco ponad 3800 m n.p.m. Do Chamonix można łatwo się dostać autostradą od strony zachodniej lub przez tunel Mont Blanc na granicy Francusko-Włoskiej.
zoom=9
center_x=45.7176863579072
center_y=6.617889404296875