description=Wyżyna Olkuska to fragment wyżyny Krakowsko-Częstochowskiej lężący na północny-zachód od Krakowa. Jest to jeden z najbardziej urokliwych miejsc Polski z charakterystycznymi skałkami z wapieni jurajskich. Najwyższe wzniesienie na Wyżynie Olkuskiej to ostaniec o	 nazwie Skałka (Grodzisko)  (512 m n.p.m.) n.p.m.)
zoom=10
center_x=50.28846183451485
center_y=19.791183471679688