description=Beskid Śląsko-Morawski to góry leżące na pograniczu Czech i Słowacji, chociaż głownie w Czechach. Góry leżą na wschód od naszego Beskidu Śląskiego za granicą Polsko-Czeską. Najwyższe szczyty to Lysá hora (Łysa Góra, 1324 m n.p.m.), Smrk (Smrek, 1276 m), Kněhyně (Kniehynia, 1256 m),Malchor (1219 m).
zoom=9
center_x=49.50
center_y=18.52