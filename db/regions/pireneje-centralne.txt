description=Pireneje Centralne leżą na granicy Hiszpańsko-Francuskiej na zachód od Andory. Region ten jest najwyższym pasmem Pirenejów. Najwyższy szczyt Pirenejów Centralnych, jak i całych Pirenejów to leżący w Hiszpanii Pico de Aneto (3404 m n.p.m.) W regionie znajduje się kilka naprawdę fantastycznych podjazdów w tym Col de Tourmalet który jest podjazdem-legendą na Tour de France.
zoom=8
center_x=42.69252994883861
center_y=0.655059814453125