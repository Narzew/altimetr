description=Alpy Zillertalskie leżą w południowo-zachodniej Austrii na granicy z Włochami. Jadąc z Polski przez Niemcy są to Alpy leżące po lewej stronie od Przełęczy Brenner. Najwyższy szczyt pasma Hochfeiler 3509m n.p.m. leżacy na granicy z Włochami. Region głównie słynie z ośrodków narciarskich. W regionie poprowadzona jest też jedna z bardziej znanych dróg wysokogórskich w Austrii Zillertaler Höhenstrasse.
zoom=10
center_x=47.14396343808789
center_y=11.784896850585938