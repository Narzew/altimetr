description=Pojezierze Leszczyńskie to  duży region naturalny w zachodniej Polsce, położony na południe od Pradoliny Warszawsko-Berlińskiej pomiędzy dolinami Odry i Warty. W skład Pojezierza Leszczyńskiego wchodzą: Pojezierze Sławskie, Pojezierze Krzywińskie, Równina Kościańska, Wał Żerkowski.
zoom=9
center_x=51.91
center_y=16.96