description=Roztocze to kraina geograficzna między wyżyną Lubelską a kotliną Sandomierską. Kraina ta to szeroki na 12-32km pas wzniesień i długości 180km aż na teren Ukrainy. Najwyższy punkt Roztoczy to szczyt o wysokości 414m n.p.m. i leży w Ukrainie, w Polsce najwyższy szczyt to Długi Goraj (391,5 m n.p.m.)
zoom=9
center_x=50.65468471250644
center_y=22.718353271484375