description=Góry Stołowe to masyw górski w Sudetach Środkowych. Są to jedne z nielicznych w Europie góry płytowe, charakteryzujące się płaskimi szczytami. Największe uzdrowiska w górach stołowych to  Kudowa-Zdrój i Polanica-Zdrój. Największym szczytem jest Szczeliniec Wielki (919 m.n.p.m.) na którym można podziwiać bardzo ciekawe formy skalne. Zachodnia część Gór Stołowych leży na terenie Czech i nosi nazwę Broumovská Vrchovina.
zoom=10
center_x=50.53
center_y=16.40