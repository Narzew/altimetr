description=Pojezierze Drawskie to jeden z ładniejszych rejonów Polski północnej. Duże jeizora są symbolem tego regionu. Ale można też spotkać liczne wzniesienia będące pozostałością po wędrujących lodowcach. Najwyższe wzniesienie regionu ma 223 m n.p.m.
zoom=9
center_x=53.6560333408472
center_y=16.217193603515625