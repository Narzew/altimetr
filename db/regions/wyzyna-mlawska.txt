description=Wzniesienia Mławskie to region w północno-środkowej Polsce, znajdujący się w połowie drogi między Warszawą a Olsztynem. Miasta regionu to Mława i Działdowo. Najwyższe wzniesienie to Dębowa Góra znajdujący się na wysokości 235 m n.p.m.
zoom=9
center_x=53.11
center_y=20.65292358398437