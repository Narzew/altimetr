description=Apeniny Toskańskie znajdują się w północnej części Włoch, na południe od niziny Padańskiej. W większym skrócie Apeniny Toskańskie leżą między Modeną i Bolonią a Florencją, Pisą, aż po wybrzeże morza Liguryjskiego. Najwyższy szczyt Apeninów Toskańskich to Monte Cimone 2156 m n.p.m. Region słynie z przepięknych zabytków Florencji, Sieny, czy Lucca. Koniecznie też trzeba zwiedzić Campo dei Miràcoli z krzywą wieżą w Pisie.
zoom=8
center_x=44.19402066387343
center_y=11.22528076171875