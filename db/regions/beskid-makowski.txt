description=Beskid Makowski to część Beskidów Zachodnich. To góry oddalone tylko 60  km od Krakowa jadąc na południe. Najwyższe szczyty to Pasmo Lubomira i Łysiny (Lubomir 904 m n.p.m.; Łysina 891 m n.p.m.) oraz Pasmo Koskowej Góry (Koskowa Góra 874 m n.p.m.; Kotoń 868 m n.p.m.) W regionie można znaleźć interesujące podjazdy min. podjazd pod Koksową Górę. Do regionu włączyłem też Pogórze Wielickie.
zoom=10
center_x=49.76
center_y=19.70