description=Pogórze Wiśnickie to region położony na południowy-wschód od Krakowa. Znajduje się nad Beskidem Wyspowym. Największe wzniesienia regionu to  Rogozowa (536 m n.p.m), Glichowiec (527 m), Szpilówka (516 m), Piekarska Góra (515 m). Największym miastem w regionie jest Bochnia.
zoom=10
center_x=49.85215166776998
center_y=20.307540893554688