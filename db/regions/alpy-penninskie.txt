description=Alpy Pennińskie nazywane również Walijskie to jedne z najwyższych pasm Alpejskich. Leżą na pograniczu Włoch i Szwajcarii. W Alpach Pennińskich jest ponad 30 szczytów powyżej 4000m n.p.m z czego najwyższy jest Dufourspitze w masywie Monte Rosa. Jego  wysokość to 4634 m n.p.m. Dufourspitze jest najwyższym szczytem w Szwajcarii.
zoom=9
center_x=45.8536734968093
center_y=7.697296142578125