description=Pogórze Śląskie to region w okolicach Bielska Białej będący przedgórzem Beskidu Małego i Śląskiego. Pogórze jest bardzo zaludnione. Główne miasta to Cieszyn, Skoczów, Bielsko-Biała, Kęty i Andrychów. Najwyższy szczyt pogórza to Jasieniowa 520 m n.p.m.
zoom=10
center_x=49.874282751903394
center_y=18.92326354980468