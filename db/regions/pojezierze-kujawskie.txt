description=Pojezierze Chełmińsko-Dobrzyńskie  znajduje się w północnej części Polski.  Krajobraz tego pojezierza jest równinny, urozmaicają go małe jeziora i malownicza dolina rzeki Drwęcy, a na wschodzie - Garb Lubawski. Najwyższym szczytem na terenie pojezierzy Chełmińsko-Dobrzyńskich jest Dylewska Góra 312 m n.p.m. Do regionu zaliczyłem też pojezierze Kujawskie, region leżący poniżej. Krajobraz pojezierza Kujawskiego jest jeszcze bardziej równinny o wysokości maksymalnej 159m n.p.m.
zoom=8
center_x=52.90
center_y=18.92