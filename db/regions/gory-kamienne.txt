description=Góry Kamienne to pasmo górskie w Sudetach Środkowych wzdłuż granicy polsko-czeskiej. Leżą one na wschód od Karkonoszy. Największym szczytem jest Waligóra 936 m.n.p.m. Dzielą się na cztery mniejsze pasma: Góry Krucze, Czarny Las, Pasmo Lesistej i Góry Suche. Do Gór Kamiennych wlicza się też Wyżynę Unisławską leżącą na południe od Wałbrzycha.
zoom=10
center_x=50.64
center_y=16.28