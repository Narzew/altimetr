description=Wzgórza Niemczańsko-Strzelińskie to region znajdujący się na południe od równiny Wrocławskiej. Najwyższe szczyty wzgórz przekraczają 400 m n.p.m. Wzgórza wchodzą w skład przedgórza Sudeckiego.
zoom=11
center_x=50.6677427443026
center_y=16.99962615966797