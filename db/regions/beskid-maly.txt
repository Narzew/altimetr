description=Beskid Mały to region Beskidów Zachodnich znajdujący się koło Bielska-Białej. Znajduje się tam kilka bardzo trudnych podjazdów z Hrobaczą Łąką, górą Żar i Magórką Wilkowicką na czele. Najwyższy szczyt Beskidu Małego to  Czupel (933 m n.p.m.)
zoom=11
center_x=49.76
center_y=19.27