description=Biokovo to góry w Chorwacji schodzące do Morza Adriatyckiego na Riwierze Makarskiej. Góry charakteryzują pięknymi stromymi wapiennymi szczytami widzianymi od strony morza. Najwyższy szczyt to Sveti Jure – 1762 m n.p.m.
zoom=9
center_x=43.51868025160499
center_y=17.016448974609375